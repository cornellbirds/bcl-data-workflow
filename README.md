# The Bird Cams Lab Project

Bird Cams Lab (BCL) was a project at the Cornell Lab of Ornithology funded by
the National Science Foundation. The BCL project created opportunities for the
public to work with scientists to create scientific investigations using the
Cornell Lab's Bird Cams. 

From 2018 to 2021, there were six investigations:

* three collected data in real time on the Bird Cams or Bird Cams Lab website;
* three collected data from archived 10-second video clips on the Zooniverse platform.  

This repository contains the code with which the BCL team analyzed the data 
from these investigations.

# Live Data Analysis

For the real-time investigations, Bird Cams Lab community made
observations on the Cornell Lab's live streaming Bird Cams. 

The R files in the ``cam_footage_live`` directory clean the data, calculate
probabilities, and produce data files for interactive visualizations. In the
case of Hawk Happenings and Cornell Feeders Live, the R code also performs
statistical analyses. These files are named after the investigations for which
they were created (e.g., ``code_cornell_feeders_live.R`` analyzes data from the
Cornell Feeders live data). 

The interactive visualizations created by this code can be found on the Bird
Cams Lab website (https://birdcamslab.allaboutbirds.org/). 

Rachael Mady is the primary author of the R code. She collaborated with Matthew
Strimas-Mackey for some of the code, and was advised by Wesley Hochachka on how
to summarize the data.

# Archived Data Analysis 

There were three investigations in which the Bird Cams Lab community made
observations on archived video in the form of 10-second clips on the [Zooniverse](https://zooniverse.org) platform.

There were two significant tasks that the team accomplished in order
to make these investigations possible: 

* Video stream footage from the Bird Cams site were captured and transformed into a format that allowed for them to be ingested into Zooniverse;
* The observation data from Zooniverse had to be reformatted and summarized in order to produce data files for interactive visualizations.

## Video Ingestion

The data captured from video stream of the Bird Cams live sites exceeded the
size limitation that Zooniverse places on uploads (1 Mb). It was therefore
necessary to segment the recordings into 10s clips. In addition, depending on
the format, quality, and duration of the recordings, it was necessary to scale
to a lower resolution or change the compression. 

The [ffmpeg](https://www.ffmpeg.org/) command, appropriately parameterized,
provided all the functionality necessary for these transformations. Scaling and
segmenting can be combined in ffmpeg, but we kept segmentation as a separate
step. Three combinations of parameters were used, each of which is discussed in
the three subsections below. 

All recordings had fluctuating frame rates due to having been streamed over the
network. Because ``ffmpeg`` uses key frames to accurately segment recordings, it
was necessary to transcode or copy while forcing key frames to ensure
consistent durations of the segmented video clips that were produced. 

The commands were executed via the command line in Ubuntu on Windows
Linux subsystem, or via batch files in Windows.  

We used batch files to process folders with thousands of longer duration
recordings, but if you have fewer recordings you can manually apply each of
these commands to process segments one a time.

### Altering Compression

Modifying the ``-crf`` parameter of ``ffmpg`` when segmenting video changes
compression and can help achieve a lower file size with reduced quality.

The value passed to the ``-crf`` parameter, which is in the range of 0 to 51, 
determines the level of compression used: the higher the value, the greater the
compression and the lesser the quality.

We recommend testing ``-crf`` values with a relatively short file and verifying
that the output is optically clear enough for your data collection while the
file size remains under the 1MB file limit. Once you have good output, try your command on a
longer file, and then proceed to running batches.

The following command forces key frames, scales to 480, sets the -crf at 35, and
creates 10s segments.

  ```
  ffmpeg -i *.mp4 -force_key_frames "expr:gte(t,n_forced*2)" -vf scale=850:480 -pix_fmt yuv420p -crf 35 -segment_time 10 -f segment -reset_timestamps 1 -segment_list manifest -segment_list_type csv *%03d.mp4
  ```

### Scaling Resolution

The following command will scale 1080p HD video to 480p and force key
frames for even segmenting. 

  ```
  for %%a in ("*.mp4") do ffmpeg -i "%%a" -vf scale=850:480 -pix_fmt yuv420p -crf 35 -force_key_frames "expr:gte(t,n_forced*2)" -y "\480s\480_%%~na.mp4"
  pause
  ```

### Segmentation

There is no transcoding in this command, just segmenting. It outputs a set of
consecutive 10s 480p .mp4 video files segmented from the original, longer video
files. It also populates a manifest for upload into Zooniverse (output
file is on C:\ ).

  ```
  for %%a in ("*.mp4") do ffmpeg -i "%%a" -c copy -force_key_frames 1
  -segment_time 10 -f segment -force_key_frames 1 -reset_timestamps 1
  -segment_list "%%a_manifest" -segment_list_type csv -y
  "\480splits\%%~na_%%03d.mp4"
  pause
  ```

## Analysis

When the observation phase on Zooniverse was completed, the participants'
observations were analyzed. Rachael worked with volunteer programmer Peter Mason
to make the Zooniverse data more usable. 

The scripts that Peter Mason created:

* Export the observation data;
* Extract the observation attributes; 
* Make the data clean and consistent;
* Apply consensus rules in order to create data files for interactive 
visualizations on the [Bird Cams Lab](https://birdcamslab.allaboutbirds.org/)
website. 

Peter Mason is the primary author of these scripts, and collaborated with Rachael
Mady to decide on appropriate filters and consensus rules. Each Python script is
named after the investigation it was created for (e.g.,

``script_battling_birds_panama`` is written for the Battling Birds: Panama data).

# Questions and Comments

If you have any questions about the project or the code, email Bird Cams (birdcams@cornell.edu). 

