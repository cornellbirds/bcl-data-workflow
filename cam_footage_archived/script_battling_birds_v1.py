# load packages that you will need to run the script;
# these come with Python and don't need to be loaded prior to running script
import csv
import json
import sys
import operator
from collections import Counter
import os
import argparse
import textwrap
from datetime import date

csv.field_size_limit(sys.maxsize)

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    fromfile_prefix_chars='@',
    description=textwrap.dedent("""
This script was written in Python 3.7.1 "out of the box" and should run without any added packages.
release version 3.0
3.01 added back in the remove stills logical condition lines 53-56
3.02 modified comments, removed redundant if's
3.03 RPM modified total species and total displacements to be the median when consensus isn't reached
4.00 Peter modified how voter fraction is calculated for whether a displacement occurred, and details of displacement
    before: No displacement if (# no birds + # no disp)/# total classif > .60
    now: No displacement if # no disp/# birds presence > .60
    before: Displacement details calculated out of total number classifications; now out of # displacement happened            
4.01 Moved to GitHub
4.02 Add a command line argument to pass the local working directory rather than hard code it.
4.03 Add command line arguments to pass the data export file, workflow and workflow version,
        add a date and simplify the output file names.
4.04 Revise means of aggregating total birds. species and displacements, and means of filtering same
        to handle totals greater than 10 
4.05 Add floating number to total birds to flag species with 10+ individuals in the total 
NOTE: You may use a file to hold the command-line arguments like:
@/path/to/args.txt."""))

parser.add_argument('--directory', '-d', required=False, default=r'C:\py\Battling_birds',
                    help="""The working directory where the input diles are found and the ouput files will be placed.
                    example -d C:\py\Battling_birds """)
parser.add_argument('--data_export_file', '-e', required=True,
                    help="""The name of the classification data export file (by workflow or in total)
                    example -e battling-birds-classifications.csv """)
parser.add_argument('--workflow', '-w', required=False, default='6152',
                    help="""The workflow number to analyze defaults to 6152)
                    example -w 6152 """)
parser.add_argument('--workflow_version', '-wv', required=False, default=277.168,
                    help="""The earliest workflow version to include defaults to 277.168,)
                    example -wv 277.168 """)

args = parser.parse_args()
directory = args.directory
data_file = args.data_export_file
workflow = args.workflow
workflow_version = args.workflow_version

# Tell python where the classification data, the stills data, and the temperature data are 
# stills.csv is a file where there is a list of subject IDs that are still images that need to be removed
location = directory + os.sep + 'battling-birds-classifications.csv'
stills = directory + os.sep + 'stills.csv'
metafile_location = directory + os.sep + 'temp_newa_cornell_2018.csv'

date = date.today().strftime("%m%d%y")

# Output file names (whatever you want them to be)
out_location = directory + os.sep + 'battling_birds_1_flattened_' + date + '.csv'  # a sort deletes this file after use
sorted_location = directory + os.sep + 'battling_birds_1_sorted_' + date + '.csv'
aggregate_location = directory + os.sep + 'battling_birds_1_aggregated_' + date + '.csv'
filtered_location = directory + os.sep + 'battling_birds_1_filtered_' + date + '.csv'
columns_location = directory + os.sep + 'battling_birds_1_columns_' + date + '.csv'

# build a list of stills in memory - these will be skipped in the final output files
with open(stills) as stills_file:
    classifications = csv.DictReader(stills_file)
    remove_stills = []
    for r_s in classifications:
        remove_stills.append(r_s['stills'])


# Function definitions needed for any blocks in this area.


def include(class_record):
    #  define a function that returns True or False based on whether the argument record is to be
    #  included or not in the output file based on the conditional clauses.
    #  many other conditions could be set up to determine if a record is to be processed and the
    #  flattened data written to the output file.

    if class_record['workflow_id'] == workflow:
        pass  # this one selects the workflow to include.
    else:
        return False
    if float(class_record['workflow_version']) >= float(workflow_version):
        pass  # this one selects the first version of the workflow to include.
    else:
        return False
    if class_record['subject_ids'] not in remove_stills:
        pass  # checks for subject on the remove_stills list
    else:
        return False
    if class_record['user_name'] != 'lavanyakarnati':
        pass  # this is to remove a specific user that appears to have entered garbage
    else:
        return False
    if '2018-11-23 00:00:00 UTC' >= class_record['created_at'] >= '2000-00-10 00:00:00 UTC':
        pass  # replace earliest and latest created_at date and times to select records commenced in a
        #  specific time period
    else:
        return False
    # otherwise :
    return True


def resolve_displacments(displace, outcome):
    # define a function that handles the displacement and outcome information 
    # handles the information differently depending on how many displacements have been catalogued.
    # also puts in "Unknown" when displacements are not entered correctly (ie only source selected)
    resolved = []  # empty
    if len(displace) == 1:
        # the normal case with interaction between one species:
        if displace[0][1] == ["SOURCE", "TARGET"] \
                or displace[0][1] == ['TARGET', 'SOURCE']:
            resolved.append([displace[0][0], displace[0][0], outcome])
        # only source defined:
        elif displace[0][1] == ["SOURCE"]:
            resolved.append([displace[0][0], "UNKNOWN", outcome])
        # only target defined:
        elif displace[0][1] == ["TARGET"]:
            resolved.append(["UNKNOWN", displace[0][0], outcome])
        # in case there is some other weird input:
        else:
            resolved.append(["NOCLUE", displace[0][0], outcome])

    elif len(displace) == 2:
        # the normal two species interaction listed source, target
        if displace[0][1] == ["SOURCE"] and displace[1][1] == ["TARGET"]:
            resolved.append([displace[0][0], displace[1][0], outcome])
        # the normal two species interaction listed target, source
        elif displace[1][1] == ["SOURCE"] and displace[0][1] == ["TARGET"]:
            resolved.append([displace[1][0], displace[0][0], outcome])
        # any other two species interaction incorrectly defined
        else:
            resolved.append(["NOCLUE", "NOCLUE", outcome])

    elif len(displace) == 3:
        # possible one source two target displacement
        if str(displace).count("SOURCE") == 1:
            for sp in range(0, 3):
                if displace[sp][1] == ["SOURCE"]:
                    resolved.append(
                        [displace[sp][0], displace[(sp + 1) % 3][0], outcome])
                    resolved.append(
                        [displace[sp][0], displace[(sp + 2) % 3][0], outcome])

    else:
        # any other incorrectly or incompletely defined displacement
        resolved.append(["NOCLUE", "NOCLUE", outcome])
    return resolved


# define the names of the columns for the output file
with open(out_location, 'w', newline='') as out_file:
    fieldnames = ['classification_id',
                  'subject_ids',
                  'filename',
                  'created_at',
                  'user_name',
                  'total_species',
                  'total_birds',
                  'total_disp',
                  'all_species',
                  'precipitation',
                  'precip_vector',
                  'displace_occurred',
                  'occurred_vector',
                  'displacements'
                  ]
    writer = csv.DictWriter(out_file, fieldnames=fieldnames)
    writer.writeheader()

    # this area for initializing counters:
    rc2 = 0
    rc1 = 0
    wc1 = 0
    #  this loads the question labels and possible responses we need to breakout the survey data.
    q1_template = ['Nope', 'no displacements', 'one or more']
    q3_template = ['Yes', 'No']
    q5_template = ['Yes', 'No']
    q6_template = ['Yes', 'No']
    question_t1 = 'WHATISTHELARGESTNUMBEROFINDIVIDUALSTHATYOUSAWSIMULTANEOUSLY'
    response_t1 = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
    question_t4 = 'WASTHISSPECIESTHESOURCETHETARGETORTHESOURCEANDTARGET'
    response_t4 = ['SOURCE', 'TARGET']

    #  open the zooniverse data file using dictreader, and load the more complex json strings
    #  as python objects using json.loads()
    with open(location) as class_file:
        classifications = csv.DictReader(class_file)
        for row in classifications:  # for each row in the classifications file
            rc2 += 1
            # useful for debugging - set the number of record to process at a low number ~1000
            # if rc2 == 150000:
            #     break
            if include(row) is True:
                rc1 += 1
                annotations = json.loads(row['annotations'])
                subject_data = json.loads(row['subject_data'])

                # reset the field variables for each new row - need to re-initialize the variables each time
                # '' defines empty string, [] defines an empty list
                # 0 reset the variable to be 0,  [0, 0, etc] defines a list with values set to 0
                q1 = ''
                q1_vector = [0, 0, 0]
                q3 = ''
                q5 = ''
                q6 = ''
                q6_vector = [0, 0]
                t1_choice = []
                species_count = 0
                individuals = 0
                displacement_count = 0
                displacements_flatten = []
                success = []
                error = ''

                # recover the subject metadata fields to be included in the output file: The metadata is
                # in a dictionary which is the value in a overall dictionary with one key "subject_ids"
                metadata = subject_data[(row['subject_ids'])]
                try:  # run the following code. if it doesn't work, then go to the "except"
                    # in this case the metadata has only one key 'filename'
                    filename = metadata['filename']
                except KeyError:
                    filename = ''

                # The next section works on the actual task blocks in the annotations column.
                # First a for loop cycles over the tasks, an if statement tests for a specific task by task number.
                # Note we could have merged these for loops  - they were repeated to have a consistent structure
                # for each task.

                # The question T0 block - displacements occurred? (possible answers: yes, no, no birds)
                for task_0 in annotations:
                    if task_0['task'] == 'T0':
                        q1 = task_0['value']  # this pulls out the response to the task in question
                        for counter in range(0, len(q1_template)):
                            # search the response for the options in the template for this task
                            if q1.find(q1_template[counter]) >= 0:
                                # when found set the output value to that option
                                q1 = q1_template[counter]
                                # this was just to convert a "Nope" to a more professional 'no birds'
                                if counter == 0:
                                    q1 = 'no birds'
                                # and finally if required, build the vector form of the output:
                                q1_vector[counter] = 1

                # The question T6 block - precipitation? (possible answers: yes or no) similar structure to T0
                for task_6 in annotations:
                    if task_6['task'] == 'T6':
                        q6 = task_6['value']
                        for counter in range(0, len(q6_template)):
                            if q6.find(q6_template[counter]) >= 0:
                                q6 = q6_template[counter]
                                q6_vector[counter] = 1

                if q1_vector[0] != 1:
                    # The survey task T1 block - all species
                    # This if statement saves looping through the tasks if we know from task T0 there are no birds
                    for task_1 in annotations:
                        if task_1['task'] == 'T1':
                            try:
                                individuals = 0  # initialize variable "individuals" which will be a count of all birds
                                # loop over each of the selections made:
                                for species in task_1['value']:
                                    try:
                                        choice = species['choice']  # a survey task always uses "choice" to store
                                        # the species, and "answers" is a dictionary with the question responses,
                                        # in our case only one question with a answer which is one of the options in
                                        # the list response_t1.
                                        count = species['answers'][question_t1]
                                        individuals += int(count)
                                        # to flag cases where the count was selected as 10+ add .001 to the sum
                                        # note zooniverse strips off the "+" from the how many choice "10+" making
                                        # it look like just "10"
                                        if count == '10':
                                            individuals += .001
                                        # store all the species and counts that were recorded
                                        t1_choice.append([choice, count])
                                    except KeyError:
                                        pass
                            except KeyError:
                                print(row['classification_id'], 'error in t1 task')
                            species_count = len(t1_choice)

                #  The question T5 block - successful? (possible answers yes or no)
                for task_5 in annotations:
                    if task_5['task'] == 'T5':
                        q5 = task_5['value']
                        # similar structure to the T) block except convert 'Yes' and 'No' to 'success', 'not successful'
                        if q5.find('Yes') >= 0:
                            q5 = 'success'
                        else:
                            q5 = 'not successful'
                        success.append(q5)

                # The survey task T4 block - displacement species:
                # Basically the same structure as Task T1 with no need to convert the answers
                for task_4 in annotations:
                    t4_choice = []
                    if task_4['task'] == 'T4':
                        displacement_count += 1
                        try:
                            for species in task_4['value']:
                                try:
                                    choice = species['choice']
                                    source = species['answers'][question_t4]
                                    # we end up with a list of the displacements recorded:
                                    t4_choice.append([choice, source])
                                except KeyError:
                                    pass
                        except KeyError:
                            print(row['classification_id'], 'error in t4 task')

                        # filter the list of displacements for those which are complete and correctly defined:
                        resolved_dis = resolve_displacments(t4_choice, success[displacement_count - 1])

                        # mark those displacements which are exact duplicates so we will not aggregate multiple
                        # events together later on. Test all new displacements in this classification against
                        # those already found for this classification, mark duplicates and add them to the list.
                        for res in range(0, len(resolved_dis)):
                            for dis in displacements_flatten:
                                if dis == resolved_dis[res]:
                                    resolved_dis[res][2] += ' multiple'

                        displacements_flatten.extend(resolved_dis)

                # Just so we can see something is happening as the script runs, print a '.' every 10000 records
                wc1 += 1
                if wc1 % 10000 == 0:
                    print('.')

                # fill in output file with the values just obtained in the above for loops 
                writer.writerow({'classification_id': row['classification_id'],
                                 'subject_ids': row['subject_ids'],
                                 'filename': filename,
                                 'created_at': row['created_at'],
                                 'user_name': row['user_name'],
                                 'total_species': species_count,
                                 'total_birds': str(individuals),
                                 'total_disp': displacement_count,
                                 'all_species': json.dumps(t1_choice),
                                 'precipitation': q6,
                                 'precip_vector': q6_vector,
                                 'displace_occurred': q1,
                                 'occurred_vector': json.dumps(q1_vector),
                                 'displacements': json.dumps(displacements_flatten)
                                 })

# This area prints some basic process info and status
print(rc2, 'lines read and inspected', rc1, 'records processed and', wc1, 'lines written')
#  ____________________________________________________________________________________________________________


# This section defines a sort function. Note the parameter used to sort where fields
# are numbered starting from '0'. This prepares the file to be aggregated and is necessary for the
# old fashion aggregation routine used.
def sort_file(input_file, output_file_sorted, field, reverse, clean):
    #  This allows a sort of the output file on a specific field.
    with open(input_file, 'r') as in_file:
        in_put = csv.reader(in_file, dialect='excel')
        headers = in_put.__next__()
        sort = sorted(in_put, key=operator.itemgetter(field), reverse=reverse)
        with open(output_file_sorted, 'w', newline='') as sorted_out:
            write_sorted = csv.writer(sorted_out, delimiter=',')
            write_sorted.writerow(headers)
            sort_counter = 0
            for line in sort:
                write_sorted.writerow(line)
                sort_counter += 1
    if clean:  # clean up temporary file
        try:
            os.remove(input_file)
        except OSError:
            print('temp file not found and deleted')
    return sort_counter


print(sort_file(out_location, sorted_location, 1, False, True), 'lines sorted and written')
#  ____________________________________________________________________________________________________


# This next section aggregates the responses for each subject and outputs the result
# with one line per subject.


def sub_total_species(specieslist):
    # counts the number of votes for each species, and records the "how many" responses in a vector format
    condensed_species = []
    species_set = set()
    for item in specieslist:
        species_set |= {item[0]}
    for element in species_set:
        votes = 0
        num = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for item in specieslist:
            if item[0] == element:
                votes += 1
                for ind in range(0, 10):
                    if item[1] == response_t1[ind]:
                        num[ind] += 1
        condensed_species.append([element, votes, num])
    return condensed_species


def sub_total_displace_exact(displacelist):
    # counts the votes for each unique displacement description recorded - keeps multiples as separate events
    condensed_displace = []
    disp_set = set()
    for item in displacelist:
        disp_set |= {str(item)}
    for element in disp_set:
        votes = 0
        list_item = []
        for item in displacelist:
            if str(item) == element:
                list_item = item
                votes += 1
        condensed_displace.append([list_item, votes])
    return condensed_displace


# open up the file to output the aggregate and set up the file writer
with open(aggregate_location, 'w', newline='') as ag_file:
    # define the column names of the output file
    fieldnames = ['subject_ids',
                  'filename',
                  'total_class',
                  'total_birds',
                  'total_species',
                  'total_disp',
                  'all_species',
                  'precipitation',
                  'displace_occurred',
                  'displacements']
    writer = csv.DictWriter(ag_file, fieldnames=fieldnames)
    # write the header
    writer.writeheader()

    # The old fashion aggregation routine with the vote count and file write
    # open the flattened file and set up the file reader
    with open(sorted_location) as so_file:
        sorted_file = csv.DictReader(so_file)
        # initialize the variables that will store the aggregated values to define them and give them some (null) value.
        subject = ''
        filename = ''
        users = set()
        class_count = 0
        total_birds = []
        total_species = []
        total_disp = []
        all_species = []
        precipitation = [0, 0]
        displace_occurred = [0, 0, 0]
        displacements_agg = []
        row_counter = 0
        write_counter = 0

        for row2 in sorted_file:  # loop over the rows in the sorted file (use 'row2' to avoid confusion with 'row')
            row_counter += 1
            new_subject = row2['subject_ids']
            new_user = {row2['user_name']}
            # if the subject has changed we are finished aggregating the previous subject so process the aggregate and
            # output the results
            if new_subject != subject:
                if row_counter != 1:  # don't want to output the empty initial values when we hit the first row
                    write_counter += 1
                    new_row = {'subject_ids': subject,
                               'filename': filename,
                               'total_class': class_count,
                               'total_birds': json.dumps(total_birds),
                               'total_species': json.dumps(total_species),
                               'total_disp': json.dumps(total_disp),
                               # subtotal over the unique species
                               'all_species': json.dumps(sub_total_species(all_species)),
                               'precipitation': json.dumps(precipitation),
                               'displace_occurred': json.dumps(displace_occurred),
                               # subtotal over the unique displacements
                               'displacements': json.dumps(sub_total_displace_exact(displacements_agg))
                               }
                    writer.writerow(new_row)
                # set up the new values to begin aggregating the next subject:
                subject = new_subject
                filename = row2['filename']
                users = new_user
                # convert individual bird counts to an aggregated vector for total birds
                total_birds = [float(row2['total_birds'])]  # use float to handle 10+ case (10.001)
                total_species = [int(row2['total_species'])]
                total_disp = [int(row2['total_disp'])]
                class_count = 1  # a counter for the number of classifications
                # load in the first classification's data
                all_species = json.loads(row2['all_species'])
                precipitation = json.loads(row2['precip_vector'])
                displace_occurred = json.loads(row2['occurred_vector'])
                displacements_agg = json.loads(row2['displacements'])
            else:
                # Test using sets for unique users, only aggregate first classification per user
                if users != users | new_user:
                    users |= new_user
                    subject = new_subject
                    # for each valid classification increment/aggregate the new data in vector format for counts
                    class_count += 1
                    for r3 in range(0, 3):
                        displace_occurred[r3] += json.loads(row2['occurred_vector'])[r3]
                    for r4 in range(0, 2):
                        precipitation[r4] += json.loads(row2['precip_vector'])[r4]
                    total_birds.append(float(row2['total_birds']))  # use float to handle 10+ case (10.001)
                    total_species.append(int(row2['total_species']))
                    total_disp.append(int(row2['total_disp']))
                    # build lists of all species and displacements
                    if len(json.loads(row2['all_species'])) > 0:
                        all_species.extend(json.loads(row2['all_species']))
                    if len(json.loads(row2['displacements'])) > 0:
                        displacements_agg.extend(json.loads(row2['displacements']))

        # catch the last aggregate after the end of the file is reached
        write_counter += 1
        new_row = {'subject_ids': subject,
                   'filename': filename,
                   'total_class': class_count,
                   'total_birds': json.dumps(total_birds),
                   'total_species': json.dumps(total_species),
                   'total_disp': json.dumps(total_disp),
                   # subtotal over the unique species
                   'all_species': json.dumps(sub_total_species(all_species)),
                   'precipitation': json.dumps(precipitation),
                   'displace_occurred': json.dumps(displace_occurred),
                   # subtotal over the unique displacements
                   'displacements': json.dumps(sub_total_displace_exact(displacements_agg))
                   }
        writer.writerow(new_row)
    print(row_counter, 'lines aggregated into', write_counter, 'subject categories')
# ___________________________________________________________________________________________________________

'''
  The next section applies a FILTER to accept a consensus by plurality or to determine if the
  result is too ambiguous to accept.  Multiple species, if they survive the filter, are output
  as elements of a list in the format [species, how many,[species v_f, howmany v_f]]. 
  Note that v_f stands for voter fraction.

 The details of the filter are as follows:

1)  The minimum number of classifications required to retain a subject as classified : 3  
        Subjects with insufficient classifications will be flagged as 'A0 insufficient classifications'
        or appear in the columnar file with only the metadata info and can be found by looking for 
        precipitation columns with a blank answer.

2)  The minimum total v_f to count any species as present : >= 60%  This applies for any number
            of that species (eg 30% say there is one species and 30% say there are two species present,
            then 60% agree that one species is present and it will be counted).
            If no species has a v_f >= 60% then mark as 'A1 no consensus for species'.
    

These limits are applied and then, of those that remain, "how many" is calculated as follows:

3)  If any single how_many bin exists for the species and is >= 60% report that how_many and the
        vote fraction for that bin as the how many v_f. Note there can be only one bin >=60%.

    If a multiple "how many" bins exist for the species (count or identification errors) but none
        meet the >=60%, then sum the v_f from the highest count downwards until at least 50% see at
        least that count (median value) and report the summed v_f (ie >= 50% saw at least that count).

4) For precipitation report "Yes" if Yes vote >= 60%, "No" if no votes >= 60% otherwise report
        "Unknown". Report the yes or no vote fraction, leave v_f blank for Unknown.

5) For the occurrence of displacements, bird presence from task T0:
         Separately test the T0 vector for displacements occurred and for the presence of birds.
         For displacements: report "Yes" if the Yes displacements vote >= 60%, "No" if No displacements votes are 
         >= 60% otherwise report "Unsure". Report the Yes or No vote fraction, leave v_f blank for Unsure. 
         Important to note that Vote fractions are calculated out of the number of votes for birds present not 
         total number of classifications 
         
         For bird presence: report "No birds" if the No birds vote is >= .60, "Yes birds" if No displacements 
         plus Yes displacements vote is >= .60, otherwise report "Unsure birds". Report the Yes or No 
         vote fraction, leave v_f blank for Unsure birds. 
        

6) For the total birds, total species, or total displacements: 
        If the vote fraction for any of the totals of the individual birds, species,
        of the total number of displacements reported is >=60%, then the total is reported 
        with its vote fraction and a "c" to indicate consensus. 
        If no one value meets consensus, then sum the v_f from the highest count downward until at least 50%
        see at least that count (median value) and report the summed v_f and a "m" to indicate median value
        (i.e. >= 50% saw at least that count). 

'''

# build a dictionary for date, time, temp in memory from file supplied keyed to filename.
with open(metafile_location) as t_file:
    temp_reader = csv.DictReader(t_file)
    temp_info = {}  # ??
    for r_m in temp_reader:
        temp_info[r_m['date_time']] = r_m['temp']


def get_date_time(file_name):
    # define a function to get the time and then date of each clip 
    s = file_name.find('s_')
    yr = file_name[s + 2: s + 6]
    mm = file_name[s + 6: s + 8]
    dd = file_name[s + 8: s + 10]
    date_ = yr + '-' + mm + '-' + dd
    time_ = file_name[s + 11: s + 13] + ':' + file_name[s + 13: s + 15]
    date_time = mm + '/' + dd + '/' + yr + ' ' + file_name[s + 11: s + 13] + ':00 EST'
    return date_, time_, date_time


def precipitation_filter(cl_tot, precip_count):
    # define a function to filter precipitation values 
    [count_yes, count_no] = precip_count
    if count_no / int(cl_tot) >= .6:
        return "No", f"{count_no / int(cl_tot):.2F}"
    elif count_yes / int(cl_tot) >= .6:
        return "Yes", f"{count_yes / int(cl_tot):.2F}"
    else:
        return "Unsure", ''


def disp_occur_filter(cl_tot, displ_count):
    # define a function to filter for displacements occurred, This filter calculated the vote fraction based
    # only on votes for Yes/No displacements, not total number of classifications.
    [count_no_birds, count_no, count_yes] = displ_count
    disp_votes = count_no + count_yes  # disp_votes is really the number of classifications for birds are present.
    result = ['', '']
    # sort out displacements
    if disp_votes / int(cl_tot) >= .60:
        if count_yes / disp_votes >= .60:
            result[0] = "Yes", f"{count_yes / disp_votes:.2F}"
        elif count_no / disp_votes >= .60:
            result[0] = "No", f"{count_no / disp_votes:.2F}"
        else:
            result[0] = "Unsure", ""
    else:
        result[0] = "", ""
    # sort out bird presence
    if count_no_birds / int(cl_tot) >= .60:
        result[1] = "No birds", f"{count_no_birds / int(cl_tot):.2F}"
    elif (count_yes + count_no) / int(cl_tot) >= .60:
        result[1] = "Yes birds", f"{(count_yes + count_no) / int(cl_tot):.2F}"
    else:
        result[1] = "Unsure birds", ""
    return result[0][0], result[1][0], result[0][1], result[1][1]


# define a function to perform the filter to calculate the total number of birds present
# the same calculation is used for total species and total displacements
def tot_birds_filter(cl_tot, total):
    counted = Counter(total).most_common()
    # the bin that equals or exceeds .60  - can only be one!
    if counted[0][1] / int(cl_tot) >= .60:
        max_v_f = round(counted[0][1] / int(cl_tot), 2)
        return counted[0][0], f"{max_v_f:.2F}" + 'c'
    sum_v_f = 0
    for cnt in sorted(counted, key=operator.itemgetter(0), reverse=True):
        # sum bins from top down until v_f exceeds .50 report 'high median' number
        sum_v_f += cnt[1] / int(cl_tot)
        if sum_v_f >= .50:
            return cnt[0], f"{sum_v_f:.2f}" + 'm'
    return ['', '']


def species_filter(cl_tot, species_list):
    # define a function to filter how many species there are 
    # Apply test - are there enough votes to count any species?
    filtered_species = []  # empty list
    sorted_species = sorted(species_list, key=operator.itemgetter(1), reverse=True)
    if len(sorted_species) == 0:
        return []
    else:
        if sorted_species[0][1] / int(cl_tot) < .60:
            return 'A1 no consensus for species'
        else:
            for item in sorted_species:
                if item[1] / int(cl_tot) >= .60:
                    num = how_many_filter(cl_tot, item[2])
                    filtered_species.append([item[0], num[0], (round(item[1] / int(cl_tot), 2), num[1])])
    return filtered_species


def how_many_filter(cl_tot, how_many):
    # define a filter to determine how many of each species there are
    sum_v_f = 0
    for idx in range(0, 10):
        # the bin that equals or exceeds .60  - can only be one!
        if how_many[idx] / int(cl_tot) >= .60:
            max_h_m = int(response_t1[idx])
            if idx == 9:
                max_h_m += .001
            max_v_f = how_many[idx] / int(cl_tot)
            return max_h_m, f"{max_v_f:.2f}" + 'c'
    # if the above consesnus condition was not met use median
    for idx in range(0, 10):
        # sum bins from top down until v_f exceeds .50 report 'median' number
        sum_v_f += how_many[9 - idx] / int(cl_tot)
        if sum_v_f >= .50:
            med_h_m = int(response_t1[9 - idx])
            if idx == 0:
                med_h_m += .001
            return med_h_m, f"{sum_v_f:.2f}" + 'm'
    return ['', '']


def displacement_filter(displ_count, disp_list):
    # define a filter for displacements
    [count_no_birds, count_no, count_yes] = displ_count
    cl_tot = count_no_birds + count_no + count_yes
    disp_votes = count_no + count_yes
    # Apply test - are there enough votes to count any displacements
    filtered_displace = []
    sorted_disp = sorted(disp_list, key=operator.itemgetter(1), reverse=True)
    if len(sorted_disp) == 0:
        return []
    else:
        if disp_votes / cl_tot >= .60:  # if  >= .60 agree that there are birds present test for displacements
            # test highest rated for consensus there are no displacements
            if sorted_disp[0][1] / disp_votes < .60 and count_yes / disp_votes < .60:
                return []
            # or there is no consensus either way
            if sorted_disp[0][1] / disp_votes < .60:
                return 'A1 no consensus for displacement(s)'
            # otherwise there is consensus for at least one displacements, test for more
            else:
                for item in sorted_disp:
                    if item[1] / disp_votes >= .60:
                        filtered_displace.append([item[0], (round(item[1] / disp_votes, 2))])
    return filtered_displace


# create the names of the columnes in the output file 
with open(filtered_location, 'w', newline='') as fi_file:
    fieldnames = ['subject_ids',
                  'filename',
                  'date',
                  'time',
                  'temp',
                  'precipitation',
                  'classifications',
                  'total_birds',
                  'total_species',
                  'species',
                  'displace_occurred',
                  'bird_presence_t0',
                  'total_disp',
                  'displacements',
                  '',
                  'precip v_f',
                  'tot_bird_v_f',
                  'tot_species_v_f',
                  'occured_v_f',
                  'bird_presence_v_f',
                  'tot_disp_v_f'
                  ]
    writer = csv.DictWriter(fi_file, fieldnames=fieldnames)
    writer.writeheader()
    with open(aggregate_location) as agg_file:
        aggregated_file = csv.DictReader(agg_file)
        rc6 = 0
        for row3 in aggregated_file:
            rc6 += 1
            subject = row3['subject_ids']
            filename = row3['filename']
            date, time, datetime = get_date_time(row3['filename'])
            temp = temp_info[datetime]
            class_totals = row3['total_class']
            tot_birds = ['', '']
            tot_species = ['', '']
            tot_disp = ['', '']
            precip = ['', '']
            species = []
            dis_ocur = ['', '', '', '']
            displacements_filt = []
            if int(class_totals) < 3:
                species = 'A0 insufficient classifications'
            else:  # apply the functions defined above to filter things
                tot_birds = tot_birds_filter(class_totals, json.loads(row3['total_birds']))[:]
                #  Note for totals, if the v_f has a flag "m" then at 50% or more of the respondents
                #  saw "at least" that number. If the tot_birds ends with multiples of .001 then the total includes
                #  the use of the 10+ bin for that multiple number of species - this makes little difference to
                #  the interpretation of the value if it is the median value, but if it is a consensus value
                #  (flag "c" on the v_f) then again the total should be considered as "at least" that number.
                tot_species = tot_birds_filter(class_totals, json.loads(row3['total_species']))[:]
                tot_disp = tot_birds_filter(class_totals, json.loads(row3['total_disp']))[:]
                #  will now filter like tot_birds to be median
                precip = precipitation_filter(class_totals, json.loads(row3['precipitation']))
                dis_ocur = disp_occur_filter(class_totals, json.loads(row3['displace_occurred']))
                if dis_ocur[1] != 'No birds':
                    species = species_filter(class_totals, json.loads(row3['all_species']))
                    displacements_filt = displacement_filter(json.loads(row3['displace_occurred']),
                                                             json.loads(row3['displacements']))
            # file the values in
            new_line = {'subject_ids': subject,
                        'filename': filename,
                        'date': date,
                        'time': time,
                        'temp': temp,
                        'precipitation': precip[0],
                        'classifications': class_totals,
                        'total_birds': tot_birds[0],
                        'total_species': tot_species[0],
                        'species': json.dumps(species),
                        'displace_occurred': dis_ocur[0],
                        'bird_presence_t0': dis_ocur[1],
                        'total_disp': tot_disp[0],
                        'displacements': json.dumps(displacements_filt),
                        'precip v_f': precip[1],
                        'tot_bird_v_f': tot_birds[1],
                        'tot_species_v_f': tot_species[1],
                        'tot_disp_v_f': tot_disp[1],
                        'occured_v_f': dis_ocur[1],
                        'bird_presence_v_f': dis_ocur[3]
                        }
            writer.writerow(new_line)

print(rc6, 'subject-choices filtered')
#  ___________________________________________________________________________________________________________________
""" This next section transforms the output to the simplified columnar format where each subject has one row still but 
    there are no voter fractions present"""
species_names = {'MOURNINGDOVE': 'modo',
                 'TUFTEDTITMOUSE': 'tuti',
                 'BLACKCAPPEDCHICKADEE': 'bcch',
                 'BLUEJAY': 'blja',
                 'AMERICANGOLDFINCH': 'amgo',
                 'HOUSEFINCH': 'hofi',
                 'DOWNYWOODPECKER': 'dowo',
                 'HAIRYWOODPECKER': 'hawo',
                 'EUROPEANSTARLING': 'eust',
                 'WHITEBREASTEDNUTHATCH': 'wbnu',
                 'NORTHERNCARDINAL': 'noca',
                 'REDWINGEDBLACKBIRD': 'rwbb',
                 'COMMONGRACKLE': 'cogr',
                 'REDBELLIEDWOODPECKER': 'rbwo',
                 'BROWNHEADEDCOWBIRD': 'bhco',
                 'CHIPPINGSPARROW': 'chsp',
                 'AMERICANTREESPARROW': 'atsp'
                 }
species_name_list = list(species_names.values())  # create a list with shortened species codes made above
disp_name_list = ['disp_1_source', 'disp_1_target', 'disp_1_succ', 'disp_2_source', 'disp_2_target',
                  'disp_2_succ', 'disp_3_source', 'disp_3_target', 'disp_3_succ', 'disp_4_source',
                  'disp_4_target', 'disp_4_succ', 'disp_5_source', 'disp_5_target', 'disp_5_succ']

fieldnames = ['subject_id', 'file_name', 'date', 'time', 'classifications','temp', 'precip', 'total_birds', 'total_species', '', ]
fieldnames.extend(species_name_list)
fieldnames.extend(['', 'displacement', 'birds_presence', 'total_disp'])
fieldnames.extend(disp_name_list)

with open(columns_location, 'w', newline='') as co_file:
    writer = csv.DictWriter(co_file, fieldnames=fieldnames)
    writer.writeheader()
    with open(filtered_location, 'r', newline='') as fi_file:
        filtered_file = csv.DictReader(fi_file)
        for row4 in filtered_file:
            dis_place = json.loads(row4['displacements'])
            species_ = json.loads(row4['species'])
            next_line = {'subject_id': row4['subject_ids'],
                         'file_name': row4['filename'],
                         'classifications': row4['classifications'],
                         'displacement': row4['displace_occurred'],
                         'birds_presence': row4['bird_presence_t0'],
                         'precip': row4['precipitation'],
                         'date': row4['date'],
                         'time': row4['time'],
                         'temp': row4['temp']
                         }
            if species_ != 'A0 insufficient classifications':
                next_line['total_birds'] = row4['total_birds']
                # see notes above for interpretation of values of total birds with decimal points
                next_line['total_species'] = row4['total_species']
                next_line['total_disp'] = row4['total_disp']
                if species_ != 'A1 no consensus for species' \
                        and len(species_) > 0:
                    for spec in species_:
                        next_line[species_names[spec[0]]] = spec[1]
            if dis_place != 'A1 no consensus for displacement(s)' \
                    and len(dis_place) > 0:
                for indx in range(0, len(dis_place)):
                    for step in range(0, 3):
                        next_line[disp_name_list[indx * 3 + step]] = dis_place[indx][0][step]
            writer.writerow(next_line)
# _______________________________________________________________________________________________________________
