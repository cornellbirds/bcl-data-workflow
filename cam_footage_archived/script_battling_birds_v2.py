# this script was written in Python 3.7.1 "out of the box" and should run without any added packages.
import csv
import json
import sys
import operator
from collections import Counter
import os
import argparse
import textwrap
from datetime import date

csv.field_size_limit(sys.maxsize)

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    fromfile_prefix_chars='@',
    description=textwrap.dedent("""
This script was written in Python 3.7.1 "out of the box" and should run without any added packages.
release version 2.0        
2.09 Moved to GitHub
2.10 Add a command line argument to pass the local working directory rather than hard code it.
2.11 Add command line arguments to pass the data export file, workflow and workflow version,
        add a date and simplify the output file names.
2.12 Error handling when displacement is incomplete (time only)
2.13 Revise means of aggregating total birds. species and displacements, and means of filtering same
        to handle totals greater than 10 
2.14 Add floating number to total bird  to flag species with 10+ individuals in the total        
NOTE: You may use a file to hold the command-line arguments like:
@/path/to/args.txt."""))

parser.add_argument('--directory', '-d', required=False, default=r'C:\py\Battling_birds',
                    help="""The working directory where the input files are found and the ouput files will be placed.
                    example -d C:\py\Battling_birds """)
parser.add_argument('--data_export_file', '-e', required=True,
                    help="""The name of the classification data export file (by workflow or in total)
                    example -e bird-interactions-2-0-classifications.csv """)
parser.add_argument('--workflow', '-w', required=False, default='8769',
                    help="""The workflow number to analyze defaults to 8769)
                    example -w 8769 """)
parser.add_argument('--workflow_version', '-wv', required=False, default=388.0,
                    help="""The earliest workflow version to include defaults to 388.0,)
                    example -wv 388.0 """)

args = parser.parse_args()
directory = args.directory
data_file = args.data_export_file
workflow = args.workflow
workflow_version = args.workflow_version

# The classification data:
location = directory + os.sep + data_file
metafile_location = directory + os.sep + 'temp_newa_cornell_2018.csv'

date = date.today().strftime("%m%d%y")

# Output file names (whatever you want them to be)
out_location = directory + os.sep + 'battling_birds_2_flattened_' + date + '.csv'  # a sort deletes this file after use
sorted_location = directory + os.sep + 'battling_birds_2_sorted_' + date + '.csv'
aggregate_location = directory + os.sep + 'battling_birds_2_aggregated_' + date + '.csv'
filtered_location = directory + os.sep + 'battling_birds_2_filtered_' + date + '.csv'
columns_location = directory + os.sep + 'battling_birds_2_columns_' + date + '.csv'


# Function definitions needed for any blocks in this area.
def include(class_record):
    #  define a function that returns True or False based on whether the argument record is to be
    #  included or not in the output file based on the conditional clauses.
    #  many other conditions could be set up to determine if a record is to be processed and the
    #  flattened data written to the output file.

    if class_record['workflow_id'] == workflow:
        pass  # this one selects the workflow to include.
    else:
        return False
    if float(class_record['workflow_version']) >= float(workflow_version):
        pass  # this one selects the first version of the workflow to include.
    else:
        return False
    if '2020-00-00 00:00:00 UTC' >= class_record['created_at'] >= '2019-03-13 00:00:00 UTC':
        pass  # replace earliest and latest created_at date and times to select records commenced in a
        #  specific time period
    else:
        return False
    # otherwise :
    return True


with open(out_location, 'w', newline='') as out_file:
    fieldnames = ['classification_id',
                  'subject_ids',
                  'filename',
                  'created_at',
                  'user_name',
                  'displacement',
                  'displacement_vector',
                  'birds_present',
                  'present_vector',
                  'total_species',
                  'total_birds',
                  'total_disp',
                  'all_species',
                  'precipitation',
                  'precip_vector',
                  'displace_occurred',
                  'occurred_vector',
                  'displacements'
                  ]
    writer = csv.DictWriter(out_file, fieldnames=fieldnames)
    writer.writeheader()

    # this area for initializing counters:
    rc2 = 0
    rc1 = 0
    wc1 = 0
    #  this loads the question labels and possible responses we need to breakout the survey data.
    q_template = ['Yes', 'No']
    question_t1 = 'WHATISTHELARGESTNUMBEROFINDIVIDUALSTHATYOUSAWSIMULTANEOUSLY'
    response_t1 = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
    t3_template = ['starts', 'Source', 'Target', 'Outcome ']

    #  open the zooniverse data file using dictreader, and load the more complex json strings
    #  as python objects using json.loads()
    with open(location) as class_file:
        classifications = csv.DictReader(class_file)
        for row in classifications:
            rc2 += 1
            # useful for debugging - set the number of record to process at a low number ~1000
            # if rc2 == 150000:
            #     break
            if include(row) is True:
                rc1 += 1
                annotations = json.loads(row['annotations'])
                subject_data = json.loads(row['subject_data'])

                # reset the field variables for each new row
                q2 = ''
                q2_vector = [0, 0]
                q0 = ''
                q0_vector = [0, 0]
                q4 = ''
                q5 = ''
                q5_vector = [0, 0]
                t1_choice = []
                species_count = 0
                individuals = 0
                displacement_count = 0
                displacements_set = set()
                displacements_list = []
                displacements_labeled = []
                success = []
                t3 = ['', '', '', '']
                multiple = 'X'

                # recover the subject metadata fields to be included in the output file:
                metadata = subject_data[(row['subject_ids'])]
                try:
                    filename = metadata['filename']
                except KeyError:
                    filename = ''
                try:
                    filename1 = metadata['Filename']
                except KeyError:
                    filename1 = ''
                filename += filename1

                # The question T0 block - birds present?
                for task_0 in annotations:
                    if task_0['task'] == 'T0':
                        q0 = task_0['value']
                        for counter in range(0, len(q_template)):
                            if q0.find(q_template[counter]) >= 0:
                                q0 = q_template[counter]
                                q0_vector[counter] = 1
                # The question T5 block - precipitation?
                for task_5 in annotations:
                    if task_5['task'] == 'T5':
                        q5 = task_5['value']
                        if q5 is not None:
                            for counter in range(0, len(q_template)):
                                if q5.find(q_template[counter]) >= 0:
                                    q5 = q_template[counter]
                                    q5_vector[counter] = 1

                # The survey task T1 block - all species
                if q0_vector[0] == 1:
                    for task_1 in annotations:
                        if task_1['task'] == 'T1':
                            try:
                                individuals = 0.0
                                for species in task_1['value']:
                                    try:
                                        choice = species['choice']
                                        count = species['answers'][question_t1]
                                        individuals += int(count)
                                        # to flag cases where the count was selected as 10+ add .001 to the sum
                                        # note zooniverse strips off the "+" from the how many choice "10+" making
                                        # it look like just "10"
                                        if count == '10':
                                            individuals += .001
                                        t1_choice.append([choice, count])
                                    except KeyError:
                                        pass
                            except KeyError:
                                print(row['classification_id'], 'error in t1 task')
                            species_count = len(t1_choice)

                # The question T2 block - displacements?
                for task_2 in annotations:
                    if task_2['task'] == 'T2':
                        q2 = task_2['value']
                        for counter in range(0, len(q_template)):
                            if q2.find(q_template[counter]) >= 0:
                                q2 = q_template[counter]
                                q2_vector[counter] = 1

                # So we have task['value'][i][select_label] are the four things we want to test where i
                # ranges from 0 to 3. When ever one of those things matches t3_template[k] (again k
                # ranges from 0 to 3.), then the part we want is t3[k] = task['value'][i]['label'].
                # So we need one loop to find the T3 tasks just like we did for all the others tasks,
                # then a pair of nested loops to loop over the four template items, testing each in turn
                # to the four elements of the task['value'].

                # The survey task T3 block - drop down displacement
                for task_3 in annotations:
                    t3 = ['', '', '', '']
                    if task_3['task'] == 'T3':
                        displacement_count += 1
                        for k in range(0, 4):  # task_3['value'][i][select_label]
                            for i in range(0, 4):
                                if task_3['value'][i]['select_label'].find(t3_template[k]) >= 0:
                                    try:
                                        t3[k] = task_3['value'][i]['label']
                                    except KeyError:
                                        t3[k] = ''
                        displacements_list.append(t3)  # if more than one displacement, will accumulate them
                        displacements_list.sort()
                for disp in displacements_list:
                    if displacements_set != displacements_set | {str(disp[1:])}:
                        displacements_set |= {str(disp[1:])}
                    else:
                        disp.append(multiple)
                        displacements_set |= {str(disp[1:])}
                        multiple += 'X'  # these are added so that subsequent identical displ are separate events
                    displacements_labeled.append(disp)
                wc1 += 1
                if wc1 % 10000 == 0:
                    print('.')
                writer.writerow({'classification_id': row['classification_id'],
                                 'subject_ids': row['subject_ids'],
                                 'filename': filename,
                                 'created_at': row['created_at'],
                                 'user_name': row['user_name'],
                                 'displacement': q2,
                                 'displacement_vector': json.dumps(q2_vector),
                                 'birds_present': q0,
                                 'present_vector': json.dumps(q0_vector),
                                 'total_species': species_count,
                                 'total_birds': individuals,
                                 'total_disp': displacement_count,
                                 'all_species': json.dumps(t1_choice),
                                 'precipitation': q5,
                                 'precip_vector': json.dumps(q5_vector),
                                 'displace_occurred': q2,
                                 'occurred_vector': json.dumps(q2_vector),
                                 'displacements': json.dumps(displacements_labeled)
                                 })

# This area prints some basic process info and status
print(rc2, 'lines read and inspected', rc1, 'records processed and', wc1, 'lines written')


#  ____________________________________________________________________________________________________________
#
# This section defines a sort function. Note the parameter used to sort where fields
# are numbered starting from '0'  This prepares the file to be aggregated and is necessary for the
# old fashion aggregation routine I use.


def sort_file(input_file, output_file_sorted, field, reverse, clean):
    #  This allows a sort of the output file on a specific field.
    with open(input_file, 'r') as in_file:
        in_put = csv.reader(in_file, dialect='excel')
        headers = in_put.__next__()
        sort = sorted(in_put, key=operator.itemgetter(field), reverse=reverse)
        with open(output_file_sorted, 'w', newline='') as sorted_out:
            write_sorted = csv.writer(sorted_out, delimiter=',')
            write_sorted.writerow(headers)
            sort_counter = 0
            for line in sort:
                write_sorted.writerow(line)
                sort_counter += 1
    if clean:  # clean up temporary file
        try:
            os.remove(input_file)
        except OSError:
            print('temp file not found and deleted')
    return sort_counter


print(sort_file(out_location, sorted_location, 1, False, True), 'lines sorted and written')


#  ____________________________________________________________________________________________________


# This next section aggregates the responses for each subject and out puts the result
# with one line per subject.
def sub_total_species(specieslist):  # creates condensed list of unique descriptions & count for each of those
    condensed_species = []
    species_set = set()
    for item in specieslist:
        species_set |= {item[0]}
    for element in species_set:
        votes = 0
        num = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for item in specieslist:
            if item[0] == element:
                votes += 1
                for ind in range(0, 10):
                    if item[1] == response_t1[ind]:
                        num[ind] += 1
        condensed_species.append([element, votes, num])
    return condensed_species


# this function makes BB2.0 and BB1.0 behave similarly - specifically the [1:] slices that ignore times
def sub_total_displace_exact(displacelist):
    # creates condensed list of unique descriptions & count for each of those (and list of times)
    condensed_displace = []
    disp_set = set()
    for item in displacelist:
        disp_set |= {str(item[1:])}
        # cut off time with [:1] slice; this first loop makes a set out of a list that only includes unique elements
    for element in disp_set:
        times = []
        list_item = []
        for item in displacelist:
            if str(item[1:]) == element:
                list_item = item[1:]
                times.append(int(item[0]))
        times.sort()
        condensed_displace.append([list_item, len(times), times])
    return condensed_displace


with open(aggregate_location, 'w', newline='') as ag_file:
    fieldnames = ['subject_ids',
                  'filename',
                  'total_class',
                  'total_birds',
                  'total_species',
                  'total_disp',
                  'all_species',
                  'precipitation',
                  'present_vector',
                  'displace_occurred',
                  'displacements']
    writer = csv.DictWriter(ag_file, fieldnames=fieldnames)
    writer.writeheader()

    # The old fashion aggregation routine with the vote count and file write
    with open(sorted_location) as so_file:
        sorted_file = csv.DictReader(so_file)
        subject = ''
        filename = ''
        users = set()
        class_count = 0
        total_birds = []
        total_species = []
        total_disp = []
        all_species = []
        precipitation = [0, 0]
        present_vector = [0, 0]
        displace_occurred = [0, 0]
        displacements_agg = []
        row_counter = 0
        write_counter = 0

        for row2 in sorted_file:
            row_counter += 1
            new_subject = row2['subject_ids']
            new_user = {row2['user_name']}
            if new_subject != subject:
                if row_counter != 1:  # don't want to output the empty initial values
                    write_counter += 1
                    new_row = {'subject_ids': subject,
                               'filename': filename,
                               'total_class': class_count,
                               'total_birds': json.dumps(total_birds),
                               'total_species': json.dumps(total_species),
                               'total_disp': json.dumps(total_disp),
                               'all_species': json.dumps(sub_total_species(all_species)),
                               'precipitation': json.dumps(precipitation),
                               'present_vector': json.dumps(present_vector),
                               'displace_occurred': json.dumps(displace_occurred),
                               'displacements': json.dumps(sub_total_displace_exact(displacements_agg))
                               }
                    writer.writerow(new_row)
                subject = new_subject
                filename = row2['filename']
                users = new_user
                total_birds = [float(row2['total_birds'])]  # use float to handle 10+ case (10.001)
                total_species = [int(row2['total_species'])]
                total_disp = [int(row2['total_disp'])]
                all_species = json.loads(row2['all_species'])
                class_count = 1
                precipitation = json.loads(row2['precip_vector'])
                present_vector = json.loads(row2['present_vector'])
                displace_occurred = json.loads(row2['occurred_vector'])
                displacements_agg = json.loads(row2['displacements'])
            else:
                if users != users | new_user:
                    users |= new_user  # |= is short for users = users | new_user
                    subject = new_subject
                    class_count += 1
                    for r2 in range(0, 2):
                        precipitation[r2] += json.loads(row2['precip_vector'])[r2]
                    for r3 in range(0, 2):
                        present_vector[r3] += json.loads(row2['present_vector'])[r3]
                    for r4 in range(0, 2):
                        displace_occurred[r4] += json.loads(row2['occurred_vector'])[r4]
                    total_birds.append(float(row2['total_birds']))  # use float to handle 10+ case (10.001)
                    total_species.append(int(row2['total_species']))
                    total_disp.append(int(row2['total_disp']))
                    if len(json.loads(row2['all_species'])) > 0:
                        all_species.extend(json.loads(row2['all_species']))
                    if len(json.loads(row2['displacements'])) > 0:
                        displacements_agg.extend(json.loads(row2['displacements']))

        # catch the last aggregate after the end of the file is reached
        write_counter += 1
        new_row = {'subject_ids': subject,
                   'filename': filename,
                   'total_class': class_count,
                   'total_birds': json.dumps(total_birds),
                   'total_species': json.dumps(total_species),
                   'total_disp': json.dumps(total_disp),
                   'all_species': json.dumps(sub_total_species(all_species)),
                   'precipitation': json.dumps(precipitation),
                   'present_vector': json.dumps(present_vector),
                   'displace_occurred': json.dumps(displace_occurred),
                   'displacements': json.dumps(sub_total_displace_exact(displacements_agg))
                   }
        writer.writerow(new_row)
    print(row_counter, 'lines aggregated into', write_counter, 'subject categories')
# ___________________________________________________________________________________________________________

'''
  The next section applies a filter to accept a consensus by plurality or to determine if the
  result is too ambiguous to accept.  Multiple species, if they survive the filter, are output
  as elements of a list in the format [species, how many,[species v_f, howmany v_f]].

 The details of the filter are as follows:

1)  The minimum number of classifications required retain a subject as classified : 3  
        Subjects with insufficient classifications will be flagged as A0 insufficient classifications
        or appear in the columnar file with only the metadata info and can be found by looking for 
        precipitation columns with a blank answer.

2)  The minimum total v_f to count any species as present : >= 60%  This applies for any number
            of that species eg 30% say there are one and 30% say there are two present
            then 60% agree that species is present and it would be counted.
            If no species has a v-f >= 60% then mark as 'A1 no consensus for species'.
    

Apply these limits then, of those that remain, calculate a "how many" as follows:

3)  If any single how_many bin exists for the species and is >= 60% report that how_many and the
        vote fraction for that bin as the how many v_f. Note there can be only one bin >=60%.

    If a multiple "how many" bins exist for the species (count or identification errors) but none
        meet the >=60%, then sum the v_f from the highest count downwards until at least 50% see at
        least that count (median value) and report the summed v_f (ie >= 50% saw at least that count).

4) For precipitation report "Yes" if Yes vote >= 60%, "No" if no votes >= 60% otherwise report
        "Unknown". Report the yes or no vote fraction, leave v_f blank for Unknown.

5) For the occurrence of displacements, bird presence from task T0:
         Separately test the T0 vector for displacements occurred and for the presence of birds.
         For displacements: report "Yes" if the Yes displacements vote >= 60%, "No" if No birds 
         plus No displacements votes are >= 60% otherwise report "Unsure". Report the Yes or No 
         vote fraction, leave v_f blank for Unsure.
         For bird presence: report "No birds" if the No birds vote is >= .60, "Yes birds" if No displacements 
         plus Yes displacements vote is >= .60, otherwise report "Unsure birds". Report the Yes or No 
         vote fraction, leave v_f blank for Unsure birds. 
        

6) For the total birds, total species, or total displacements: 
        If the vote fraction for any of the totals of the individual birds, species,
        of the total number of displacements reported is >=60%, then the total is reported 
        with its vote fraction and a "c" to indicate consensus. 
        If no one value meets consensus, then sum the v_f from the highest count downward until at least 50%
        see at least that count (median value) and report the summed v_f and a "m" to indicate median value
        (i.e. >= 50% saw at least that count). 

'''
# build a dictionary for date, time, temp in memory  from file supplied keyed to filename.
with open(metafile_location) as t_file:
    temp_reader = csv.DictReader(t_file)
    temp_info = {}
    for r_m in temp_reader:
        temp_info[r_m['date_time'][:-4]] = r_m['temp']  # the [:-4] cuts off the EST/EDT part


def get_date_time(file_name):
    s = file_name.find('s_')
    yr = file_name[s + 2: s + 6]
    mm = file_name[s + 6: s + 8]
    dd = file_name[s + 8: s + 10]
    date_ = yr + '-' + mm + '-' + dd
    time_ = file_name[s + 11: s + 13] + ':' + file_name[s + 13: s + 15]
    date_time = mm + '/' + dd + '/' + yr + ' ' + file_name[s + 11: s + 13] + ':00'
    return date_, time_, date_time


# filter for precipitation values
def precipitation_filter(cl_tot, precip_count):
    [count_yes, count_no] = precip_count
    if count_no / int(cl_tot) >= .6:
        return "No", f"{count_no / int(cl_tot):.2F}"
    elif count_yes / int(cl_tot) >= .6:
        return "Yes", f"{count_yes / int(cl_tot):.2F}"
    else:
        return "Unsure", ''


# filter for bird presence
def bird_pres_filter(cl_tot, present_count):
    [count_yes, count_no] = present_count
    if count_no / int(cl_tot) >= .60:
        return "No birds", f"{count_no / int(cl_tot):.2F}"
    elif count_yes / int(cl_tot) >= .60:
        return "Yes birds", f"{count_yes / int(cl_tot):.2F}"
    else:
        return "Unsure birds", ""


# filter for displacements occurred
def disp_occur_filter(cl_tot, displ_count):
    [count_yes, count_no] = displ_count
    disp_votes = count_no + count_yes  # disp_votes is total of yes and no; might not always = # votes for birds pres
    # sort out displacements
    if disp_votes / int(cl_tot) >= .60:
        if count_yes / disp_votes >= .60:
            return "Yes", f"{count_yes / disp_votes:.2F}"
        elif count_no / disp_votes >= .60:
            return "No", f"{count_no / disp_votes:.2F}"
        else:
            return "Unsure", ""
    else:
        return "", ""


# filter for total birds, species, or displacements
def tot_birds_filter(cl_tot, total):
    counted = Counter(total).most_common()
    # the bin that equals or exceeds .60  - can only be one!
    if counted[0][1] / int(cl_tot) >= .60:
        max_v_f = round(counted[0][1] / int(cl_tot), 2)
        return counted[0][0], f"{max_v_f:.2F}" + 'c'
    sum_v_f = 0
    for cnt in sorted(counted, key=operator.itemgetter(0), reverse=True):
        # sum bins from top down until v_f exceeds .50 report 'high median' number
        sum_v_f += cnt[1] / int(cl_tot)
        if sum_v_f >= .50:
            return cnt[0], f"{sum_v_f:.2f}" + 'm'
    return ['', '']


# filter for species
def species_filter(cl_tot, species_list):
    # Apply test - are there enough votes to count any species?
    filtered_species = []
    sorted_species = sorted(species_list, key=operator.itemgetter(1), reverse=True)
    if len(sorted_species) == 0:
        return []
    else:
        if sorted_species[0][1] / int(cl_tot) < .60:
            return 'A1 no consensus for species'
        else:
            for item in sorted_species:
                if item[1] / int(cl_tot) >= .60:
                    num = how_many_filter(cl_tot, item[2])
                    filtered_species.append([item[0], num[0], (round(item[1] / int(cl_tot), 2), num[1])])
    return filtered_species


# filter for species how many
def how_many_filter(cl_tot, how_many):
    sum_v_f = 0
    for idx in range(0, 10):
        # the bin that equals or exceeds .60  - can only be one!
        if how_many[idx] / int(cl_tot) >= .60:
            max_h_m = int(response_t1[idx])
            if idx == 9:
                max_h_m += .001
            max_v_f = how_many[idx] / int(cl_tot)
            return max_h_m, f"{max_v_f:.2f}" + 'c'
    for idx in range(0, 10):
        # sum bins from top down until v_f exceeds .50 report 'median' number
        sum_v_f += how_many[9 - idx] / int(cl_tot)
        if sum_v_f >= .50:
            med_h_m = int(response_t1[9 - idx])
            if idx == 0:
                med_h_m += .001
            return med_h_m, f"{sum_v_f:.2f}" + 'm'
    return ['', '']


# filter for displacements
def displacement_filter(cl_tot, displ_count, disp_list):
    [count_yes, count_no] = displ_count
    disp_votes = count_no + count_yes
    # Apply test - are there enough votes to count any displacements
    filtered_displace = []
    sorted_disp = sorted(disp_list, key=operator.itemgetter(1), reverse=True)
    if len(sorted_disp) == 0:
        return []
    else:
        if disp_votes / int(cl_tot) >= .60:  # resolve displacements only if enough people voted on the question
            if sorted_disp[0][1] / disp_votes < .60 <= count_no / disp_votes:
                # 2 reasons for no consensus - 1) there were no displ and we know that (count_no/disp_votes >=.60
                # 2) we don't know there was no disp for certain, ie there were displ
                # but even one with highest count not resolved
                return []
            if sorted_disp[0][1] / disp_votes < .60:  # [0][1] because it is a list of lists right now
                return 'A1 no consensus for displacement(s)'
            else:
                for item in sorted_disp:
                    if item[1] / disp_votes >= .60:
                        filtered_displace.append([item[0], (round(item[1] / disp_votes, 2)), item[2]])
    return filtered_displace


with open(filtered_location, 'w', newline='') as fi_file:
    fieldnames = ['subject_ids',
                  'filename',
                  'date',
                  'time',
                  'temp',
                  'precipitation',
                  'classifications',
                  'total_birds',
                  'total_species',
                  'species',
                  'bird_presence_t0',
                  'displace_occurred',
                  'total_disp',
                  'displacements',
                  '',
                  'precip v_f',
                  'tot_bird_v_f',
                  'tot_species_v_f',
                  'bird_presence_v_f',
                  'occured_v_f',
                  'tot_disp_v_f'
                  ]
    writer = csv.DictWriter(fi_file, fieldnames=fieldnames)
    writer.writeheader()
    with open(aggregate_location) as agg_file:
        aggregated_file = csv.DictReader(agg_file)
        rc6 = 0
        for row3 in aggregated_file:
            rc6 += 1
            subject = row3['subject_ids']
            filename = row3['filename']
            date, time, datetime = get_date_time(row3['filename'])
            temp = temp_info[datetime]
            class_totals = row3['total_class']
            tot_birds = ['', '']
            tot_species = ['', '']
            tot_disp = ['', '']
            bird_pres = ['', '']
            precip = ['', '']
            species = []
            dis_ocur = ['', '']
            displacements_filt = []
            if int(class_totals) < 3:
                species = 'A0 insufficient classifications'
            else:
                tot_birds = tot_birds_filter(class_totals, json.loads(row3['total_birds']))[:]
                #  Note for totals, if the v_f has a flag "m" then at 50% or more of the respondents
                #  saw "at least" that number. If the tot_birds ends with multiples of .001 then the total includes
                #  the use of the 10+ bin for that multiple number of species - this makes little difference to
                #  the interpretation of the value if it is the median value, but if it is a consensus value
                #  (flag "c" on the v_f) then again the total should be considered as "at least" that number.
                tot_species = tot_birds_filter(class_totals, json.loads(row3['total_species']))[:]
                tot_disp = tot_birds_filter(class_totals, json.loads(row3['total_disp']))[:]
                #  will now filter like tot_birds and tot species to be median
                precip = precipitation_filter(class_totals, json.loads(row3['precipitation']))
                bird_pres = bird_pres_filter(class_totals, json.loads(row3['present_vector']))
                if bird_pres[0] != 'No birds':
                    dis_ocur = disp_occur_filter(class_totals, json.loads(row3['displace_occurred']))
                    species = species_filter(class_totals, json.loads(row3['all_species']))
                    displacements_filt = displacement_filter(class_totals, json.loads(row3['displace_occurred']),
                                                             json.loads(row3['displacements']))
            new_line = {'subject_ids': subject,
                        'filename': filename,
                        'date': date,
                        'time': time,
                        'temp': temp,
                        'precipitation': precip[0],
                        'classifications': class_totals,
                        'total_birds': tot_birds[0],
                        'total_species': tot_species[0],
                        'species': json.dumps(species),
                        'bird_presence_t0': bird_pres[0],
                        'displace_occurred': dis_ocur[0],
                        'total_disp': tot_disp[0],
                        'displacements': json.dumps(displacements_filt),
                        'precip v_f': precip[1],
                        'tot_bird_v_f': tot_birds[1],
                        'tot_species_v_f': tot_species[1],
                        'tot_disp_v_f': tot_disp[1],
                        'bird_presence_v_f': bird_pres[1],
                        'occured_v_f': dis_ocur[1]
                        }
            writer.writerow(new_line)

print(rc6, 'subject-choices filtered')
#  ___________________________________________________________________________________________________________________
""" This next section transforms the output to the simplified columnar format with no v_fraction info"""
species_names = {'MOURNINGDOVE': 'modo',
                 'TUFTEDTITMOUSE': 'tuti',
                 'BLACKCAPPEDCHICKADEE': 'bcch',
                 'BLUEJAY': 'blja',
                 'AMERICANGOLDFINCH': 'amgo',
                 'HOUSEFINCH': 'hofi',
                 'DOWNYWOODPECKER': 'dowo',
                 'HAIRYWOODPECKER': 'hawo',
                 'EUROPEANSTARLING': 'eust',
                 'WHITEBREASTEDNUTHATCH': 'wbnu',
                 'NORTHERNCARDINAL': 'noca',
                 'REDWINGEDBLACKBIRD': 'rwbb',
                 'COMMONGRACKLE': 'cogr',
                 'REDBELLIEDWOODPECKER': 'rbwo',
                 'BROWNHEADEDCOWBIRD': 'bhco',
                 'CHIPPINGSPARROW': 'chsp',
                 'AMERICANTREESPARROW': 'atsp'
                 }
species_name_list = list(species_names.values())
disp_name_list = ['disp_1_source', 'disp_1_target', 'disp_1_succ', 'disp_2_source', 'disp_2_target',
                  'disp_2_succ', 'disp_3_source', 'disp_3_target', 'disp_3_succ', 'disp_4_source',
                  'disp_4_target', 'disp_4_succ', 'disp_5_source', 'disp_5_target', 'disp_5_succ']

fieldnames = ['subject_id', 'file_name', 'date', 'time', 'classifications', 'temp', 'precip', 'total_birds',
              'total_species', '', ]
fieldnames.extend(species_name_list)
fieldnames.extend(['', 'birds_presence', 'displacement', 'total_disp'])
fieldnames.extend(disp_name_list)

with open(columns_location, 'w', newline='') as co_file:
    writer = csv.DictWriter(co_file, fieldnames=fieldnames)
    writer.writeheader()
    with open(filtered_location, 'r', newline='') as fi_file:
        filtered_file = csv.DictReader(fi_file)
        for row4 in filtered_file:
            dis_place = json.loads(row4['displacements'])
            species_ = json.loads(row4['species'])
            next_line = {'subject_id': row4['subject_ids'],
                         'file_name': row4['filename'],
                         'classifications': row4['classifications'],
                         'birds_presence': row4['bird_presence_t0'],
                         'displacement': row4['displace_occurred'],
                         'precip': row4['precipitation'],
                         'date': row4['date'],
                         'time': row4['time'],
                         'temp': row4['temp']
                         }
            if species_ != 'A0 insufficient classifications':
                next_line['total_birds'] = row4['total_birds']
                # see notes above for interpretation of values of total birds with decimal points
                next_line['total_species'] = row4['total_species']
                next_line['total_disp'] = row4['total_disp']
                if species_ != 'A1 no consensus for species' \
                        and len(species_) > 0:
                    for spec in species_:
                        next_line[species_names[spec[0]]] = spec[1]
            if dis_place != 'A1 no consensus for displacement(s)' \
                    and len(dis_place) > 0:
                for indx in range(0, len(dis_place)):
                    for step in range(0, 3):
                        next_line[disp_name_list[indx * 3 + step]] = dis_place[indx][0][step]
            writer.writerow(next_line)
# _______________________________________________________________________________________________________________
