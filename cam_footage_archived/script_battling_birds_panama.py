# this script was written in Python 3.7.1 "out of the box" and should run without any added packages.
import csv
import json
import sys
import operator
from collections import Counter
from datetime import datetime, timedelta
import os
import argparse
import textwrap
from datetime import date

csv.field_size_limit(sys.maxsize)

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    fromfile_prefix_chars='@',
    description=textwrap.dedent("""
This script was written in Python 3.7.1 "out of the box" and should run without any added packages.
Release version 2.7        

2.2 -  modify to handle change to T0 task - specifically the change from "Yes.", "No." to "Yes", "No" and 
       dropping the Live Highlight and humans present options, handle metadata variations, fix time calculation
       from variations in filename, modify "AX" warnings and their output format. 
2.3 -  modify food types to handle change in responses, default workflow version to 811. to cut off at that change.
2.4 -  bugfix for handling json strings through to column format 
2.5 -  add parameter option for basing total_disp on the number of Yes votes for birds_visible
2.6 -  bugfix for birds_visible consensus
2.7 -  remove basis options, add displacements occurred, and reduce displacement description to source,target,outcome,
       rework some points in the filter primarily reporting of species and displacements only if there is consensus
       disp_occurred, and the vote fraction to report a food type increased from 50% to 60%.       
                      
NOTE: You may use a file to hold the command-line arguments like:
@/path/to/args.txt."""))

parser.add_argument('--directory', '-d', required=False, default=r'C:\py\Battling_Panama',
                    help="""The working directory where the input files are found and the output files will be placed.
                    example -d C:\py\Battling_Panama """)
parser.add_argument('--data_export_file', '-e', required=True,
                    help="""The name of the classification data export file (by workflow or in total)
                    example -e v1-total_class.csv """)
parser.add_argument('--workflow', '-w', required=False, default='17026',
                    help="""The workflow number to analyze defaults to 17026
                    example -w 17026 """)
parser.add_argument('--workflow_version', '-wv', required=False, default=811.0,
                    help="""The earliest workflow version to include defaults to 811.0,
                    example -wv 774.0 """)
parser.add_argument('--gold_standard', '-g', required=False, default=False,
                    help="""To include only gold standard total_class,  defaults to False,
                    example -g False """)
args = parser.parse_args()
directory = args.directory
data_file = args.data_export_file
workflow = args.workflow
workflow_version = args.workflow_version
gold = args.gold_standard

# The classification data:
location = directory + os.sep + data_file

date = date.today().strftime("%m%d%y")
if gold:
    date += 'gold_standard'

# Output file names (whatever you want them to be)
out_location = directory + os.sep + 'battling_birds_panama_flattened_' + date + '.csv'  # this file deleted after use
sorted_location = directory + os.sep + 'battling_birds_panama_sorted_' + date + '.csv'
aggregate_location = directory + os.sep + 'battling_birds_panama_aggregated_' + date + '.csv'
filtered_location = directory + os.sep + 'battling_birds_panama_filtered_' + date + '.csv'
columns_location = directory + os.sep + 'battling_birds_panama_columns_' + date + '.csv'


# Function definitions needed for any blocks in this area.
def include(class_record):
    #  define a function that returns True or False based on whether the argument record is to be
    #  included or not in the output file based on the conditional clauses.
    #  many other conditions could be set up to determine if a record is to be processed and the
    #  flattened data written to the output file.

    if class_record['workflow_id'] == workflow:
        pass  # this one selects the workflow to include.
    else:
        return False
    if float(class_record['workflow_version']) >= float(workflow_version):
        pass  # this one selects the first version of the workflow to include.
    else:
        return False
    if gold:
        if not class_record['gold_standard']:
            return False  # this selects gold standard
    else:
        pass
    if int(class_record['subject_ids']) < 55680000 and \
            int(class_record['subject_ids']) not in [55345016, 55345218, 55388741]:
        pass  # this removes some specific subjects known to be problems
    else:
        return False
    # if '2020-00-00 00:00:00 UTC' >= class_record['created_at'] >= '2020-00-00 00:00:00 UTC':
    #     pass  # replace earliest and latest created_at date and times to select records commenced in a
    #     #  specific time period
    # else:
    #     return False
    # otherwise :
    return True


with open(out_location, 'w', newline='') as out_file:
    fieldnames = ['classification_id',
                  'subject_ids',
                  'filename',
                  'created_at',
                  'user_name',
                  'birds_visible',
                  'visible_vector',
                  'file_colour',
                  'colour_vector',
                  'food_types',
                  'food_vector',
                  'total_species',
                  'total_birds',
                  'total_disp',
                  'all_species',
                  'displacement_occurred',
                  'occurred_vector',
                  'displacement_1',
                  'displacement_2',
                  'displacement_3',
                  'displacement_4',
                  'displacements'
                  ]
    writer = csv.DictWriter(out_file, fieldnames=fieldnames)
    writer.writeheader()

    # this area for initializing counters:
    rc2 = 0
    rc1 = 0
    wc1 = 0
    #  this loads the question labels and possible responses we need to breakout the survey data.
    q0_template = ['Yes', 'No']
    q1_template = ['Yes', 'No']
    q8_template = ['Color', 'Black']
    q11_template = ['Fruit', 'Other', 'No food']
    question_t6 = 'WHATISTHELARGESTNUMBEROFINDIVIDUALSTHATYOUSAWSIMULTANEOUSLY'
    response_t6 = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
    displacement_tasks = ['T7', 'T9', 'T10', 'T12']

    #  open the zooniverse data file using dictreader, and load the more complex json strings
    #  as python objects using json.loads()
    with open(location) as class_file:
        total_class = csv.DictReader(class_file)
        for row in total_class:
            rc2 += 1
            # useful for debugging - set the number of record to process at a low number ~1000
            # if rc2 == 150000:
            #     break
            if include(row) is True:
                rc1 += 1
                annotations = json.loads(row['annotations'])
                subject_data = json.loads(row['subject_data'])

                # reset the field variables for each new row
                q0 = ''  # birds visible
                q0_vector = [0, 0]
                q1 = ''
                q1_vector = [0, 0]
                q8 = ''  # file colour
                q8_vector = [0, 0]
                q11 = []  # food
                q11_vector = [0, 0, 0]
                t6_choice = []
                species_count = 0
                individuals = 0
                displacement_count = 0
                d = ['', '', '', '']
                displacements_set = set()
                displacements_list = []
                displacements_labeled = []
                multiple = 'X'

                # recover the subject metadata fields to be included in the output file:
                metadata = subject_data[(row['subject_ids'])]
                try:
                    filename = metadata['Filename']
                except KeyError:
                    try:
                        filename = metadata['ï»¿Filename']
                    except KeyError:
                        try:
                            filename = metadata['filename']
                        except KeyError:
                            filename = ''
                            print(row['subject_ids'], 'No filename found', metadata)

                for task in annotations:

                    # The question T0 block - birds_visible?
                    if task['task'] == 'T0':
                        q0 = task['value']
                        for counter in range(0, len(q0_template)):
                            if q0.find(q0_template[counter]) >= 0:
                                q0 = q0_template[counter]
                                q0_vector[counter] = 1

                                # The question T1 block - displacement occurred?
                    if task['task'] == 'T1':
                        q1 = task['value']
                        for counter in range(0, len(q1_template)):
                            if q1.find(q1_template[counter]) >= 0:
                                q1 = q1_template[counter]
                                q1_vector[counter] = 1

                    # The question T8 block - file_colour?
                    if task['task'] == 'T8':
                        q8 = task['value']
                        if q8 is not None:
                            for counter in range(0, len(q8_template)):
                                if q8.find(q8_template[counter]) >= 0:
                                    q8 = q8_template[counter]
                                    q8_vector[counter] = 1

                    # The question T11 block - foods present?
                    if task['task'] == 'T11':
                        if task['value'] is not None:
                            for counter in range(0, len(q11_template)):
                                for food in task['value']:
                                    if food.find(q11_template[counter]) >= 0:
                                        q11.append(q11_template[counter])
                                        q11_vector[counter] = 1

                    # The survey task T6 block - all species
                    if task['task'] == 'T6':
                        try:
                            individuals = 0
                            for species in task['value']:
                                try:
                                    choice = species['choice']
                                    count = species['answers'][question_t6]
                                    individuals += int(count)
                                    t6_choice.append([choice, count])
                                except KeyError:
                                    pass
                        except KeyError:
                            print(row['classification_id'], 'error in t6 task')
                        species_count = len(t6_choice)

                    # The question T1, T5, T13, T14 blocks - how many displacements?
                    if task['task'] in ['T1', 'T5', 'T13', 'T14']:
                        if task['value'].find('Yes') >= 0:
                            displacement_count += 1

                for i in range(0, displacement_count):
                    d[i] = [0, '', '', '', '']
                    for disp_task in annotations:
                        if disp_task['task'] == displacement_tasks[i]:
                            d[i][0] = int(disp_task['value'][0]['label'])
                            for k in range(1, 5):  # disp_task['value'][k]['label'] are the five dropdown descriptors
                                try:
                                    d[i][k] = disp_task['value'][k]['label']
                                except KeyError:
                                    d[i][k] = ''
                            displacements_list.append(d[i])  # if more than one displacement, will accumulate them
                displacements_list.sort()
                # label subsequential identical displacements
                for disp in displacements_list:
                    if displacements_set != displacements_set | {str(disp[1:4])}:
                        displacements_set |= {str(disp[1:4])}
                    else:
                        disp.append(multiple)
                        displacements_set |= {str(disp[1:4])}
                        multiple += 'X'  # these are added so that subsequent identical displ are separate events
                    displacements_labeled.append(disp)

                if q0 == 'No':  # set default disp_occurred as 'No' for No birds visible
                    q1 = 'No'
                    q1_vector = [0, 1]

                wc1 += 1
                if wc1 % 10000 == 0:
                    print('.', end='')
                writer.writerow({'classification_id': row['classification_id'],
                                 'subject_ids': row['subject_ids'],
                                 'filename': filename,
                                 'created_at': row['created_at'],
                                 'user_name': row['user_name'],
                                 'birds_visible': q0,
                                 'visible_vector': json.dumps(q0_vector),
                                 'file_colour': q8,
                                 'colour_vector': json.dumps(q8_vector),
                                 'food_types': q11,
                                 'food_vector': json.dumps(q11_vector),
                                 'total_species': species_count,
                                 'total_birds': individuals,
                                 'total_disp': displacement_count,
                                 'all_species': json.dumps(t6_choice),
                                 'displacement_occurred': q1,
                                 'occurred_vector': json.dumps(q1_vector),
                                 'displacement_1': d[0],
                                 'displacement_2': d[1],
                                 'displacement_3': d[2],
                                 'displacement_4': d[3],
                                 'displacements': json.dumps(displacements_labeled)
                                 })

# This area prints some basic process info and status
print('\n')
print(rc2, 'lines read and inspected', rc1, 'records processed and', wc1, 'lines written')


#  ____________________________________________________________________________________________________________
#
# This section defines a sort function. Note the parameter used to sort where fields
# are numbered starting from '0'  This prepares the file to be aggregated and is necessary for the
# old fashion aggregation routine I use.


def sort_file(input_file, output_file_sorted, field, reverse, clean):
    #  This allows a sort of the output file on a specific field.
    with open(input_file, 'r') as in_file:
        in_put = csv.reader(in_file, dialect='excel')
        headers = in_put.__next__()
        sort = sorted(in_put, key=operator.itemgetter(field), reverse=reverse)
        with open(output_file_sorted, 'w', newline='') as sorted_out:
            write_sorted = csv.writer(sorted_out, delimiter=',')
            write_sorted.writerow(headers)
            sort_counter = 0
            for line in sort:
                write_sorted.writerow(line)
                sort_counter += 1
    if clean:  # clean up temporary file
        try:
            os.remove(input_file)
        except OSError:
            print('temp file not found and deleted')
    return sort_counter


print(sort_file(out_location, sorted_location, 1, False, True), 'lines sorted and written')


#  ____________________________________________________________________________________________________


# This next section aggregates the responses for each subject and out puts the result
# with one line per subject.
def sub_total_species(specieslist):  # creates condensed list of unique descriptions & count for each of those
    condensed_species = []
    species_set = set()
    for item in specieslist:
        species_set |= {item[0]}
    for element in species_set:
        votes = 0
        num = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for item in specieslist:
            if item[0] == element:
                votes += 1
                for ind in range(0, 15):
                    if item[1] == response_t6[ind]:
                        num[ind] += 1
        condensed_species.append([element, votes, num])
    return condensed_species


# this function makes BB1.0, BB2.0 and BB_Panama behave similarly - specifically the [1:4] slices that ignore times
# and displacement type
def sub_total_displace_exact(displacelist):
    # creates condensed list of unique descriptions & count for each of those (and list of times)
    condensed_displace = []
    disp_set = set()
    for item in displacelist:
        disp_set |= {str(item[1:4]) + str(item[5:])}
        # this first loop makes a set out of a list that only includes unique elements for source, target, and outcome
    for element in disp_set:
        times = []
        disp_type = [0, 0, 0]
        list_item = []
        for item in displacelist:
            if str(item[1:4]) + str(item[5:]) == element:
                list_item = item[1:4]
                times.append(int(item[0]))
                if item[4].find('No') >= 0:
                    disp_type[1] += 1
                elif item[4].find('Physical') >= 0:
                    disp_type[0] += 1
                else:
                    disp_type[2] += 1
        times.sort()
        condensed_displace.append([list_item, len(times), times, disp_type])
    return condensed_displace


with open(aggregate_location, 'w', newline='') as ag_file:
    fieldnames = ['subject_ids',
                  'filename',
                  'total_class',
                  'disp_class',
                  'birds_visible',
                  'total_birds',
                  'total_species',
                  'all_species',
                  'disp_occurred',
                  'total_disp',
                  'disp_descrip',
                  'file_colour',
                  'food_types'
                  ]

    writer = csv.DictWriter(ag_file, fieldnames=fieldnames)
    writer.writeheader()

    # The old fashion aggregation routine with the vote count and file write
    with open(sorted_location) as so_file:
        sorted_file = csv.DictReader(so_file)
        subject = ''
        users = set()
        filename = ''
        birds_visible_agg = [0, 0]
        disp_occurred_agg = [0, 0]
        colour_agg = [0, 0]
        food_agg = [0, 0, 0]
        class_count = 0
        total_birds = []
        total_species = []
        total_disp = []
        all_species = []
        disp_class = 0
        disp_descrip_agg = []
        row_counter = 0
        write_counter = 0

        for row2 in sorted_file:
            row_counter += 1
            new_subject = row2['subject_ids']
            new_user = {row2['user_name']}
            if new_subject != subject:
                if row_counter != 1:  # don't want to output the empty initial values
                    write_counter += 1
                    new_row = {'subject_ids': subject,
                               'filename': filename,
                               'birds_visible': json.dumps(birds_visible_agg),
                               'file_colour': json.dumps(colour_agg),
                               'food_types': json.dumps(food_agg),
                               'total_class': class_count,
                               'disp_class': disp_class,
                               'total_birds': json.dumps(total_birds),
                               'total_species': json.dumps(total_species),
                               'total_disp': json.dumps(total_disp),
                               'all_species': json.dumps(sub_total_species(all_species)),
                               'disp_occurred': json.dumps(disp_occurred_agg),
                               'disp_descrip': json.dumps(sub_total_displace_exact(disp_descrip_agg))
                               }
                    writer.writerow(new_row)
                subject = new_subject
                users = new_user
                filename = row2['filename']
                colour_agg = json.loads(row2['colour_vector'])
                food_agg = json.loads(row2['food_vector'])
                birds_visible_agg = json.loads(row2['visible_vector'])
                total_disp = [int(row2['total_disp'])]
                total_birds = [int(row2['total_birds'])]
                total_species = [int(row2['total_species'])]
                if total_disp[0] > 0:
                    disp_class = 1
                else:
                    disp_class = 0
                all_species = json.loads(row2['all_species'])
                class_count = 1
                disp_occurred_agg = json.loads(row2['occurred_vector'])
                disp_descrip_agg = json.loads(row2['displacements'])
            else:
                if users != users | new_user:
                    users |= new_user  # |= is short for users = users | new_user
                    subject = new_subject
                    class_count += 1
                    for r2 in range(0, 2):
                        colour_agg[r2] += json.loads(row2['colour_vector'])[r2]
                    for r3 in range(0, 3):
                        food_agg[r3] += json.loads(row2['food_vector'])[r3]
                    for r4 in range(0, 2):
                        birds_visible_agg[r4] += json.loads(row2['visible_vector'])[r4]
                    for r5 in range(0, 2):
                        disp_occurred_agg[r5] += json.loads(row2['occurred_vector'])[r5]
                    if len(json.loads(row2['all_species'])) > 0:
                        all_species.extend(json.loads(row2['all_species']))
                    if len(json.loads(row2['displacements'])) > 0:
                        disp_class += 1
                        disp_descrip_agg.extend(json.loads(row2['displacements']))
                    total_birds.append(int(row2['total_birds']))
                    total_species.append(int(row2['total_species']))
                    total_disp.append(int(row2['total_disp']))
        # catch the last aggregate after the end of the file is reached
        write_counter += 1
        new_row = {'subject_ids': subject,
                   'filename': filename,
                   'birds_visible': json.dumps(birds_visible_agg),
                   'file_colour': json.dumps(colour_agg),
                   'food_types': json.dumps(food_agg),
                   'total_class': class_count,
                   'disp_class': disp_class,
                   'total_birds': json.dumps(total_birds),
                   'total_species': json.dumps(total_species),
                   'total_disp': json.dumps(total_disp),
                   'all_species': json.dumps(sub_total_species(all_species)),
                   'disp_occurred': json.dumps(disp_occurred_agg),
                   'disp_descrip': json.dumps(sub_total_displace_exact(disp_descrip_agg))
                   }
        writer.writerow(new_row)
    print(row_counter, 'lines aggregated into', write_counter, 'subject categories')
# ___________________________________________________________________________________________________________

'''
  The next section applies a filter to accept a consensus by plurality or to determine if the
  result is too ambiguous to accept.  Multiple species, if they survive the filter, are output
  as elements of a list in the format [species, how many,[species v_f, howmany v_f]].
  
 The details of the filter are as follows:  

1)  The minimum number of total_class required retain a subject as classified : 3 unless the gold
        standard parameter is set to True in which case minimum number of total_class is 1.
        Subjects with insufficient total_class will be flagged as A0 insufficient total_class
        or appear in the columnar file with only the metadata info. 
        
2)  Calculation of vote fraction: due to the filtering nature of the Battling Birds Panama workflow, species,
        displacements, file colour and food types can only be reported for classifications that first report 
        birds_visible and then report at least one displacement. For consensus of any these fields, it is logical
        to base the vote fraction only on the number of classifications that were actually able to vote on the 
        values of those fields. However this is a departure from the use of the total classifications for all 
        consensus calculations as was done in BB1 and BB2. The following table lists the classifications used to 
        calculate vote fraction for each variable. Note total_class = all classifications, visible_class = those
        that reported Yes for birds-visible and disp_class = those that reported any displacement. 
        
        birds_visible       total_class
        disp_occurred (Yes) visible_class
        disp_occurred (No)  total_class (a vote for No birds is by default a vote No displacements)
        total_disp	        visible_class
        total_birds         disp_class
        total_species       disp_class
        species             disp_class
        disp_descrip        disp_class
        file_colour         disp_class
        food_types          disp_class
        
3)  For the question birds visible, the consensus is calculated based the total classifications with "Yes" meaning 
        60% or more reported birds visible, "No" means 60% or more reported No birds visible, and "Unsure" meaning 
        there was no consensus for Yes or No.       

4)  For the question did a displacement occur, the vote fractions for "Yes" is based on the number of Yes votes 
        for birds present with consensus set at 60% or more. For "No" displacements _ occurred, the vote fraction 
        for consensus is 60% or more of the total classifications reported No birds present or No displacements if 
        birds were present.  Those cases that do not have consensus for Yes or No for displacements occurred are 
        reported as "Unsure" and in that case species is also reported as "A1 insufficient displacement votes" and 
        no other variables are reported.

5)  For the total birds, total species, or total displacements:
        If the vote fraction for any of the totals of the individual birds, species,
        of the total number of displacements reported is >=60%, then the total is reported
        with its vote fraction and a "c" to indicate consensus.
        If no one value meets consensus, then sum the v_f from the highest count downward until at least 50%
        see at least that count (median value) and report the summed v_f and a "m" to indicate median value
        (i.e. >= 50% saw at least that count).
        
6)  The minimum total v_f to count any species as present : >= 60%  This applies for any number
        of that species eg 30% say there are one and 30% say there are two present
        then 60% agree that species is present and it would be counted.
        If no species has a v-f >= 60% then mark as 'A2 no consensus for species'.

7)  Apply these limits then, of those species that remain, calculate a species "how many" as follows: 
               
        If any single how_many bin exists for the species and is >= 60% report that how_many and the
            vote fraction for that bin as the how many v_f. Note there can be only one bin >=60%.
    
        If a multiple "how many" bins exist for the species (count or identification errors) but none
            meet the >=60%, then sum the v_f from the highest count downwards until at least 50% see at
            least that count (median value) and report the summed v_f (ie >= 50% saw at least that count).

8) For the actual displacement descriptions - to report any displacement descriptions, at least 60% of the
        classifications that report a displacement must agree on source, target, and outcome of the description. 
        If no displacement description meets this criteria report  'A3 no consensus for displacement(s)' 
        The times for the displacements are reported in a list and the type (Physical Contact,  No Physical
        Contact, or Not reported) is shown as a list of the counts for each of the three possible responses.
        
9) For file_type ( colour or B&W)  report "Colour" if Colour vote >= 60%, "B&W" if Black and White votes >= 60% 
        otherwise report "Unknown". Since this field generally has excellent agreement, no vote fraction is reported.
        
10) For the food types simply calculate the vote fraction for each food type based on the total number
        of total_class where a displacement was recorded.  Report all food types that exceed 60% vf.

'''


def get_date_time(file_name_):
    p = file_name_.find('p_')
    yr = file_name_[p + 2: p + 6]
    mm = file_name_[p + 6: p + 8]
    dd = file_name_[p + 8: p + 10]
    date_ = yr + '-' + mm + '-' + dd
    base_time_ = file_name_[p + 11: p + 13] + ':' + file_name_[p + 14: p + 16] + ':' + file_name_[p + 17: p + 19]

    try:
        end = file_name_.find('.mp4')
        if file_name_[p + 19] == '_':
            add_seconds = int(file_name_[p + 20: end]) * 10
        else:
            add_seconds = int(file_name_[p + 19: end]) * 10
        file_time = datetime.strptime(date_ + ' ' + base_time_, '%Y-%m-%d %H:%M:%S') + timedelta(seconds=add_seconds)
        return file_time.strftime("%Y-%m-%d"), file_time.strftime("%H:%M:%S")
    except ValueError:
        return date_, ''


# filter for file_type
def file_type_filter(class_, type_count):
    [count_col, count_bw] = type_count
    if count_bw / class_ >= .6:
        return "B&W", f"{count_bw / class_:.2F}"
    elif count_col / class_ >= .6:
        return "Color", f"{count_col / class_:.2F}"
    else:
        return "Unsure", ''


# calculate vf for food_types
def cal_fraction(class_, food_types_):
    for q2 in range(0, len(food_types_)):
        food_vf = int(food_types_[q2] / class_ * 100 + .45)
        if food_vf >= 60:
            food_types_[q2] = food_vf
        else:
            food_types_[q2] = ''
    return food_types_


# filter for total birds, species, or displacements
def tot_birds_filter(class_, total):
    total = [item_ for item_ in total if item_ != 0]
    counted = Counter(total).most_common()
    # the bin that equals or exceeds .60  - can only be one!
    if counted[0][1] / class_ >= .60:
        max_v_f = round(counted[0][1] / class_, 2)
        return counted[0][0], f"{max_v_f:.2F}" + 'c'
    sum_v_f = 0
    for cnt in sorted(counted, key=operator.itemgetter(0), reverse=True):
        # sum bins from top down until v_f exceeds .50 report 'high median' number
        sum_v_f += cnt[1] / class_
        if sum_v_f >= .50:
            return cnt[0], f"{sum_v_f:.2f}" + 'm'
    return [0, "100+m"]  # specifically handles total_disp for consensus for 0 displacements


# filter for species
def species_filter(class_, species_list):
    # Apply test - are there enough votes to count any species?
    filtered_species = []
    sorted_species = sorted(species_list, key=operator.itemgetter(1), reverse=True)
    if len(sorted_species) == 0:
        return []
    else:
        if sorted_species[0][1] / class_ < .60:
            return 'A2 no consensus for species'
        else:
            for item in sorted_species:
                if item[1] / class_ >= .60:
                    num = how_many_filter(class_, item[2])
                    filtered_species.append([item[0], num[0], (round(item[1] / class_, 2), num[1])])
    return filtered_species


# filter for species how many
def how_many_filter(class_, how_many):
    sum_v_f = 0
    for idx in range(0, 15):
        # the bin that equals or exceeds .60  - can only be one!
        if how_many[idx] / class_ >= .60:
            max_h_m = int(response_t6[idx])
            max_v_f = how_many[idx] / class_
            return max_h_m, f"{max_v_f:.2f}" + 'c'
    for idx in range(0, 15):
        # sum bins from top down until v_f exceeds .50 report 'median' number
        sum_v_f += how_many[14 - idx] / class_
        if sum_v_f >= .50:
            med_h_m = int(response_t6[14 - idx])
            return med_h_m, f"{sum_v_f:.2f}" + 'm'
    return ['', '']


# filter for displacements
def displacement_filter(class_, disp_list):
    # Unlike BB1 and BB2 we only get to filtering displacements if there are enough displacements recorded
    # to test them for consensus, so this filter is now much simpler.
    filtered_displace = []
    sorted_disp = sorted(disp_list, key=operator.itemgetter(1), reverse=True)
    if sorted_disp[0][1] / class_ < .60:  # [0][1] because it is a list of lists right now
        return 'A3 no consensus for displacement(s)'
    else:
        for item in sorted_disp:
            if item[1] / class_ >= .60:
                filtered_displace.append([item[0], (round(item[1] / class_, 2)),
                                          item[2], item[3]])
    return filtered_displace


with open(filtered_location, 'w', newline='') as fi_file:
    fieldnames = ['subject_ids',
                  'filename',
                  'date',
                  'time',
                  'total_class',
                  'disp_class',
                  'birds_visible',
                  'total_birds',
                  'total_species',
                  'species',
                  'disp_occurred',
                  'total_disp',
                  'disp_descrip',
                  '',
                  'file_colour',
                  'food_types',
                  '',
                  'tot_bird_v_f',
                  'tot_species_v_f',
                  'tot_disp_v_f'
                  ]
    writer = csv.DictWriter(fi_file, fieldnames=fieldnames)
    writer.writeheader()
    if gold:
        min_class = 1
    else:
        min_class = 3

    with open(aggregate_location) as agg_file:
        aggregated_file = csv.DictReader(agg_file)
        rc6 = 0
        for row3 in aggregated_file:
            rc6 += 1
            total_class = int(row3['total_class'])
            disp_class = int(row3['disp_class'])
            birds_yes_no = json.loads(row3['birds_visible'])
            disp_yes_no = json.loads(row3['disp_occurred'])
            visible = ''
            occurred = ''
            visible_class = birds_yes_no[0]

            date, time = get_date_time(row3['filename'])

            if birds_yes_no[0] / total_class >= .60:  # note the vf basis for birds_visible is always total_class
                visible = q0_template[0]
            elif birds_yes_no[1] / total_class >= .60:
                visible = q0_template[1]
            else:
                visible = 'Unsure'

            if disp_yes_no[1] / total_class >= .60:  # for No disp_occurred can use total_class since No birds visible
                # defaults to also No displacements occurred
                occurred = q1_template[1]
            elif disp_yes_no[0] / visible_class >= .60:  # here we use the Yes bird visible count
                occurred = q1_template[0]
            else:
                occurred = 'Unsure'

            new_line = {'subject_ids': row3['subject_ids'],
                        'filename': row3['filename'],
                        'date': date,
                        'time': time,
                        'total_class': total_class,
                        'disp_class': disp_class,
                        'birds_visible': visible,
                        'disp_occurred': occurred
                        }

            if total_class < min_class:
                species = 'A0 insufficient total_class'
                displacements = []
                food_types = []
            else:
                if occurred == 'Yes':
                    tot_birds = tot_birds_filter(disp_class, json.loads(row3['total_birds']))[:]
                    #  Note for totals, if the v_f has a flag "m" then at 50% or more of the respondents
                    #  saw "at least" that number. If the v_f has a flag "c" it is a consensus value.
                    tot_species = tot_birds_filter(disp_class, json.loads(row3['total_species']))[:]
                    species = species_filter(disp_class, json.loads(row3['all_species']))
                    tot_disp = tot_birds_filter(visible_class, json.loads(row3['total_disp']))[:]
                    disp_descrip = displacement_filter(disp_class, json.loads(row3['disp_descrip']))
                    new_line['total_birds'] = tot_birds[0]
                    new_line['total_species'] = tot_species[0]
                    new_line['total_disp'] = tot_disp[0]
                    new_line['file_colour'] = file_type_filter(disp_class, json.loads(row3['file_colour']))[0]
                    food_types = cal_fraction(disp_class, json.loads(row3['food_types']))
                    new_line['tot_bird_v_f'] = tot_birds[1]
                    new_line['tot_species_v_f'] = tot_species[1]
                    new_line['tot_disp_v_f'] = tot_disp[1]
                elif occurred == 'No':
                    disp_descrip = []
                    food_types = []
                    species = []
                else:
                    species = 'A1 insufficient displacement votes'
                    disp_descrip = []
                    food_types = []
            new_line['species'] = json.dumps(species)
            new_line['disp_descrip'] = json.dumps(disp_descrip)
            new_line['food_types'] = json.dumps(food_types)
            writer.writerow(new_line)

print(rc6, 'subjects filtered')
#  ___________________________________________________________________________________________________________________
""" This next section transforms the output to the simplified columnar format with no v_fraction info"""
species_names = {'BANANAQUIT': 'Bananaquit', 'BLACKCHESTEDJAY': 'Black-chested Jay',
                 'BLUEGRAYTANAGER': 'Blue-gray Tanager', 'BUFFTHROATEDSALTATOR': 'Buff-throated Saltator',
                 'CHESTNUTHEADEDOROPENDOLA': 'Chestnut-headed Oropendola', 'CLAYCOLOREDTHRUSH': 'Clay-colored Thrush',
                 'COLLAREDARACARI': 'Collared Aracari', 'CRIMSONBACKEDTANAGER': 'Crimson-backed Tanager',
                 'DUSKYFACEDTANAGER': 'Dusky-faced Tanager', 'FLAMERUMPEDTANAGER': 'Flame-rumped Tanager',
                 'GRAYCOWLEDWOODRAIL': 'Gray-cowled Wood-Rail', 'GRAYHEADEDCHACHALACA': 'Gray-headed Chachalaca',
                 'GREENHONEYCREEPER': 'Green Honeycreeper', 'ORANGEBILLEDSPARROW': 'Orange-billed Sparrow',
                 'PALMTANAGER': 'Palm Tanager', 'REDCROWNEDANTTANAGER': 'Red-crowned Ant-Tanager',
                 'REDCROWNEDWOODPECKER': 'Red-crowned Woodpecker', 'REDLEGGEDHONEYCREEPER': 'Red-legged Honeycreeper',
                 'RUFOUSMOTMOT': 'Rufous Motmot', 'SPOTCROWNEDBARBET': 'Spot-crowned Barbet',
                 'SUMMERTANAGER': 'Summer Tanager', 'TENNESSEEWARBLER': 'Tennessee Warbler',
                 'THICKBILLEDEUPHONIA': 'Thick-billed Euphonia', 'WHITELINEDTANAGER': 'White-lined Tanager',
                 'HUMMINGBIRDSPECIES': 'Hummingbird species'}

species_name_list = list(species_names.values())
disp_descrip_list = ['disp_1_source', 'disp_1_target', 'disp_1_succ', 'disp_1_time', 'disp_1_type',
                     'disp_2_source', 'disp_2_target', 'disp_2_succ', 'disp_2_time', 'disp_2_type',
                     'disp_3_source', 'disp_3_target', 'disp_3_succ', 'disp_3_time', 'disp_3_type',
                     'disp_4_source', 'disp_4_target', 'disp_4_succ', 'disp_4_time', 'disp_4_type']

fieldnames = ['subject_id', 'file_name', 'date', 'time', 'total_class', 'disp_class',
              'birds_visible', 'total_birds', 'total_species', '', ]
fieldnames.extend(species_name_list)
fieldnames.extend(['', 'file_colour'])
fieldnames.extend(q11_template)
fieldnames.extend(['disp_occurred', 'total_disp'])
fieldnames.extend(disp_descrip_list)

with open(columns_location, 'w', newline='') as co_file:
    writer = csv.DictWriter(co_file, fieldnames=fieldnames)
    writer.writeheader()
    with open(filtered_location, 'r', newline='') as fi_file:
        filtered_file = csv.DictReader(fi_file)
        for row4 in filtered_file:
            species_ = json.loads(row4['species'])
            disp_descrip = json.loads(row4['disp_descrip'])
            food_types = json.loads(row4['food_types'])

            next_line = {'subject_id': row4['subject_ids'],
                         'file_name': row4['filename'],
                         'date': row4['date'],
                         'time': row4['time'],
                         'total_class': row4['total_class'],
                         'disp_class': row4['disp_class'],
                         'birds_visible': row4['birds_visible'],
                         'disp_occurred': row4['disp_occurred']
                         }
            if row4['file_colour']:
                next_line['file_colour'] = row4['file_colour']
                next_line['total_birds'] = row4['total_birds']
                next_line['total_species'] = row4['total_species']
                next_line['total_disp'] = row4['total_disp']
                if species_ != 'A2 no consensus for species' \
                        and len(species_) > 0:
                    for spec in species_:
                        next_line[species_names[spec[0]]] = spec[1]
                for ind3, food in enumerate(q11_template):
                    next_line[food] = food_types[ind3]
                if disp_descrip != 'A3 no consensus for displacement(s)' \
                        and len(disp_descrip) > 0:
                    for ind3 in range(0, len(disp_descrip)):
                        for step in range(0, 3):
                            next_line[disp_descrip_list[ind3 * 5 + step]] = disp_descrip[ind3][0][step]
                        next_line[disp_descrip_list[ind3 * 5 + 3]] = disp_descrip[ind3][2]
                        next_line[disp_descrip_list[ind3 * 5 + 4]] = disp_descrip[ind3][3]
            writer.writerow(next_line)
# _______________________________________________________________________________________________________________
