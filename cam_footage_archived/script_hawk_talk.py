""" This is a modified script developed by PmasonFF and Rachel Mady. It takes a local copy of the Zooniverse
Classification download file as input and provides a framework to which various code blocks are added 
to flatten the JSON format for the annotations field into a more user friendly format. Which code blocks 
are required depends on tasks in the specific project which generated the classification download.  
This code also provides an easy way to select specific records based on values of various fields in 
the standard classification record.  The output file structure will be modified as blocks are added, 
splitting "annotations" out into separate columns. Once certain fields in the original file are no 
longer needed (metadata, subject_data and annotations) they can be removed to reduce the file size 
(usually by an order of magnitude or more)."""

# this script was written in Python 3.7 "out of the box" and should run without any added packages.
import csv  # package for importing csv-formatted files
import json  # package for working JSON format (the format of the annotations in Zooniverse data file)
import sys  # package to allow Python to work with the host Operating system
import os  # package that allows Python to use many of the host's Operating system commands.
# Used here for supplying the correct os.sep, and deleting a file
import operator  # extends the Python sort flexibility to allow sort based on selected keys
import argparse  # Enables command line argument handling (used to pass the user directory name)
import textwrap  # pretties up the help output
from datetime import date

csv.field_size_limit(sys.maxsize)

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    fromfile_prefix_chars='@',
    description=textwrap.dedent("""
This script was written in Python 3.7.1 "out of the box" and should run without any added packages.
release version 1.0        
1.0 Moved to GitHub
1.01 Add a command line argument to pass the local working directory rather than hard code it.
1.02 Add command line arguments to pass the data export file, workflow and workflow version,
        add a date and simplify the output file names.
NOTE: You may use a file to hold the command-line arguments like:
@/path/to/args.txt."""))

parser.add_argument('--directory', '-d', required=False, default=r'C:\py\Hawk_Talk',
                    help="""The working directory where the input files are found and the ouput files will be placed.
                    example -d C:\py\Hawk_Talk """)
parser.add_argument('--data_export_file', '-e', required=True,
                    help="""The name of the classification data export file (by workflow or in total)
                    example -e v5-classifications-082319.csv """)
parser.add_argument('--workflow', '-w', required=False, default='8448',
                    help="""The workflow number to analyze defaults to 8448)
                    example -w 8448 """)
parser.add_argument('--workflow_version', '-wv', required=False, default=90.201,
                    help="""The earliest workflow version to include defaults to 90.201,)
                    example -wv 90.201 """)

args = parser.parse_args()
directory_path = args.directory
data_file = args.data_export_file
workflow = args.workflow
workflow_version = args.workflow_version

# set up full input data file path and name
location = directory_path + os.sep + data_file

date = date.today().strftime("%m%d%y")

# Output file names (whatever you want them to be)
out_location = directory_path + os.sep + 'hawk_talk_flattened_' + date + '.csv'  # a sort deletes this file after use
sorted_location = directory_path + os.sep + 'hawk_talk_sorted_' + date + '.csv'
aggregate_location = directory_path + os.sep + 'hawk_talk_aggregated_' + date + '.csv'
filtered_location = directory_path + os.sep + 'hawk_talk_filtered_' + date + '.csv'
columns_location = directory_path + os.sep + 'hawk_talk_columns_' + date + '.csv'


# Define a function that returns True or False based on whether the argument record is to be included or not in
# the output file based on the conditional clauses.
# (NOTE - in Python, need two spaces before and after function definitions)
def include(class_record):
    #  many other conditions could be set up to determine if a record is to be processed and the flattened data
    #  written to the output file. Any or all of these conditional tests that are not needed can be deleted or
    # commented out with '#' placed in front of the line(s) of code that are not required.

    if class_record['workflow_id'] == workflow:  # int() allows Python to read the workflow ID correctly as an integer
        pass  # this one selects the workflow to include.
    else:
        return False

    if float(class_record['workflow_version']) >= float(workflow_version):
        # float() allows Python to read the workflow version, a decimal, correctly
        pass  # specify the first version of the workflow to include.
    else:
        return False

    # if 100000000 >= int(class_record['subject_ids']) >= 10000000:
    #     pass  # replace upper and lower subject_ids to include only a specified range of subjects
    #     # can set upper end much higher so that all subjects classified are included
    # else:
    #     return False
    #
    # if '2100-00-10 00:00:00 UTC' >= class_record['created_at'] >= '2000-00-10 00:00:00 UTC':
    #     pass  # replace earliest and latest created_at date and times to select records commenced in a
    #     #  specific time period
    # else:
    #     return False

    # otherwise if all conditions are met then:
    return True


# Prepare the output file and write the header
with open(out_location, 'w', newline='') as file:
    # The list of field names must include each field required in the output. The names, and order must be exactly
    # the same here as in the writer statement near the end of the program. The names and order are arbitary -
    # your choice, as long as they are the same in both locations.
    # As code blocks are added to flatten the annotations JSON, columns need to be added to contain each newly
    # split out group of data. Add each one using the format: 'new_field_name', 
    # Similarly fields can be removed from both places to reduce the file size if the information is not needed 
    # for the current purpose.
    fieldnames = ['classification_id',
                  'user_name',
                  'workflow_id',
                  'workflow_name',
                  'workflow_version',
                  'filename',
                  'created_at',
                  'annotations',
                  'subject_data',
                  'subject_ids',
                  'date',
                  'time',
                  'who',
                  'what',
                  'nestling_vocal',
                  'adult_vocal',
                  'answer_vector']
    writer = csv.DictWriter(file, fieldnames=fieldnames)
    writer.writeheader()

    # Set the variable values at 0 to "initialize them" for use later 
    i = 0
    j = 0

    # Loads the question labels
    q1_template = ['Nestlings', 'One Adult', 'Two Adults', 'No Hawks']
    q2_template = ['Adult brooding', 'Adult arriving or departing', 'Adult feeding nestlings',
                   'Adult eating', 'Adult changing prey location', 'Adult transfering prey',
                   'Prey is visible', 'None of the above']
    q3_template = ['No', 'Short peep', 'High-pitched whistle', 'Other']
    q4_template = ['No', 'Kee-eeee-arrr', 'Chwirk', 'Gank', 'Other']

    # Open the Zooniverse data file using DictReader), and load the more complex JSON strings as Python objects
    with open(location) as f:
        r = csv.DictReader(f)
        for row in r:
            i += 1
            # useful for debugging - set the number of record to process at a low number ~1000
            # completely arbitrary number set so only a few records are flattened before script stops
            # entirely for development purposes so we don't process the whole file every time we try the script
            # Uncomment the below if need to use
            # if i == 7000:  # one more than the last line of zooniverse file read if not EOF
            #   break
            if include(row) is True:  # Note include() function was first defined function at the beginning of code
                j += 1
                # once we begin working with these JSON format columns we will need to load the json
                # strings as Python objects (dictionaries and lists) by removing the '#' from these next 2 lines:
                annotations = json.loads(row['annotations'])
                subject_data = json.loads(row['subject_data'])

                # Reset(initialize) field variables for each new row
                q1 = []  # Empty brackets define an empty list
                q1_vector = [0, 0, 0, 0]
                q2 = []
                q2_vector = [0, 0, 0, 0, 0, 0, 0, 0]
                q3 = []
                q3_vector = [0, 0, 0, 0, 0]
                q4 = []
                q4_vector = [0, 0, 0, 0, 0, 0]

                # Recover the subject metadata fields to be included in the output file:
                metadata = subject_data[(row['subject_ids'])]
                try:
                    filename = metadata['filename']
                except KeyError:
                    filename = ''

                # Get the DATE from the subject metadata
                # The date is the part following PTZ_ for the next ten characters
                start = filename.find('PTZ_')
                date = filename[start + 4:start + 14]

                # Get the HOUR from the subject metadata 
                hour = int(filename[start + 15: start + 17])
                # some_string[m:n] gives us part of string beginning w/ character position given by integer m
                # and ending with character before but not including character position given by integer n
                # Get the MINUTE from the subject metadata
                minute = int(filename[start + 18: start + 20])

                # Get the number of how many 10 second video clips past the hour-minute time in the file name
                # that were created
                number = int(filename[start + 21: start + 24])
                # ex: first one is 000 so number*10 is total seconds to the start of the clip past the hour/minute
                # shown in the filename

                seconds = (number * 10) % 60  # Returns the remainder (becomes the seconds)
                # when we divide total seconds by 60
                new_minutes = (minute + int(number * 10 / 60)) % 60  # Returns the remainer (becomes the new minutes)
                new_hours = hour + int(
                    (minute + int(number * 10 / 60)) / 60)  # Returns the new hours considering the number of clips

                # Check to see if clips extended into next day; Most likely didn't,  since usable clips are
                # in daylight hours
                if new_hours >= 24:
                    print('time runs into next day!!!!')

                # Piece together the new_hours, new_minutes, and seconds variables to create a single time 
                time = str(new_hours).zfill(2) + ':' + str(new_minutes).zfill(2) + ':' + str(seconds).zfill(2)

                # Break down answers from the question T2 block - WHO IS VISIBLE?
                # Use "append" method because multiple answers are allowed
                for task_0 in annotations:
                    if task_0['task'] == 'T2':
                        list_q1 = task_0['value']
                        for counter in range(0, len(q1_template)):
                            for item in list_q1:
                                if item.find(q1_template[counter]) >= 0:
                                    q1.append(q1_template[counter])
                                    q1_vector[counter] = 1

                # Break down answers to question T6 block - WHAT IS HAPPENING?
                for task_1 in annotations:
                    if task_1['task'] == 'T6':
                        list_q2 = task_1['value']
                        for counter in range(0, len(q2_template)):
                            for item in list_q2:
                                if item.find(q2_template[counter]) >= 0:
                                    q2.append(q2_template[counter])
                                    q2_vector[counter] = 1

                #  Break down answers to T3 block - CAN YOU HEAR A NESTLING VOCALIZING?
                for task_2 in annotations:
                    if task_2['task'] == 'T3':
                        list_q3 = task_2['value']
                        for counter in range(0, len(q3_template)):
                            for item in list_q3:
                                if item.find(q3_template[counter]) >= 0:
                                    q3.append(q3_template[counter])
                                    q3_vector[counter] = 1
                        some_vocal = 0
                        for counter in range(1, len(q3_template)):
                            some_vocal += q3_vector[counter]
                        if some_vocal > 0:
                            q3_vector[4] = 1

                            # Break down answers to T4 block - CAN YOU HEAR AN ADULT VOCALIZING?
                for task_3 in annotations:
                    if task_3['task'] == 'T4':
                        list_q4 = task_3['value']
                        for counter in range(0, len(q4_template)):
                            for item in list_q4:
                                if item.find(q4_template[counter]) >= 0:
                                    q4.append(q4_template[counter])
                                    q4_vector[counter] = 1
                        some_vocal = 0
                        for counter in range(1, len(q4_template)):
                            some_vocal += q4_vector[counter]
                        if some_vocal > 0:
                            q4_vector[5] = 1

                # Record what was just pulled out in an output file
                writer.writerow({'classification_id': row['classification_id'],
                                 'user_name': row['user_name'],
                                 'workflow_id': row['workflow_id'],
                                 'workflow_name': row['workflow_name'],
                                 'workflow_version': row['workflow_version'],
                                 'created_at': row['created_at'],
                                 'filename': filename,
                                 'annotations': row['annotations'],
                                 'subject_data': row['subject_data'],
                                 'subject_ids': row['subject_ids'],
                                 'date': date,
                                 'time': time,
                                 'who': json.dumps(q1),
                                 # json.dumps() tells csv file writer to write lists so they load as lists
                                 # and not strings. # NOTE: lists can store any type of data (integers, characters,
                                 # strings) while strings can only store a set of characters
                                 # We want it to be a true list of individual string items, not just one big
                                 # string for when we use this information further down in the code.
                                 'what': json.dumps(q2),
                                 'nestling_vocal': json.dumps(q3),
                                 'adult_vocal': json.dumps(q4),
                                 'answer_vector': json.dumps([q1_vector, q2_vector, q3_vector, q4_vector])})

                print(j)  # just so we know progress is being made

        print(i, 'lines read and inspected', j, 'records processed and written')
    # ______________________________________________________________________________________________________________


# This section defines a sort function. Note the parameter used to sort where fields
# are numbered starting from '0' This prepares the file to be aggregated and is necessary for the
# old fashion aggregation routine PmasonFF uses.

def sort_file(input_file, output_file_sorted, field, reverse, clean):
    # This (? the function?) allows a sort of the output file on a specific field.
    with open(input_file, 'r') as in_file:
        in_put = csv.reader(in_file, dialect='excel')
        headers = in_put.__next__()
        sort = sorted(in_put, key=operator.itemgetter(field), reverse=reverse)
        with open(output_file_sorted, 'w', newline='') as sorted_out:
            write_sorted = csv.writer(sorted_out, delimiter=',')
            write_sorted.writerow(headers)
            sort_counter = 0
            for line in sort:
                write_sorted.writerow(line)
                sort_counter += 1
    if clean:  # clean up temporary file
        try:
            os.remove(input_file)
        except OSError:
            print('temp file not found and deleted')
    return sort_counter


print(sort_file(out_location, sorted_location, 9, False, False), 'lines sorted and written')


# The number in the sort_file() defines the column that will be used to sort the file and needs to be
# the subject_ids column. Note in Python things get counted from 0 so classifications column is 0 and
# subject ids column is 9
# _____________________________________________________________________________________________________________________


# This next section AGGREGATES the responses for each subject and outputs the result with one line per subject.
# get_vote_fraction() is a function with parameters class count and aggregated vector
# The function is placed above where it is needed with two blank lines before and two blank lines after
# Note - frowned upon (and confusing) to use same variable names, so use variations of the names in the function

# We defined a blank vector for the results we are calculating called v_f_vector,
# Run "for" loops to get each value,
# do the calculation, format result for some reasonable appearance,
# return result once loops are finished
# We have specified the formatting here to make it so that 2 decimal places are used

# Important note - up until 04/18/19, the third and fourth question in workflow were not functioning properly and 
# participants were able to click through without responding; 
# Current aggregation setup is such that the NO RESPONSE classifications are still counted 
# so that the classification count goes up (and vote fraction of any actual responses goes down)


def get_vote_fraction(cl_count, ag_vector):
    v_f_vector = []
    for b in range(0, len(ag_vector)):
        v_f_vector.append([])
        for c in range(0, len(ag_vector[b])):
            v_f_vector[b].append(f"{(ag_vector[b][c] / cl_count): .2F}")
    return v_f_vector


with open(aggregate_location, 'w', newline='') as ag_file:
    fieldnames = ['subject_ids',
                  'filename',
                  'date',
                  'time',
                  'total_class',
                  'aggregated_vector',
                  'vote_fraction_vector']
    writer = csv.DictWriter(ag_file, fieldnames=fieldnames)
    writer.writeheader()

    # The old fashion aggregation routine with the vote count and file write
    with open(sorted_location) as so_file:
        sorted_file = csv.DictReader(so_file)
        subject = ''
        filename = ''
        date = ''
        time = ''
        users = set()  # defines an empty set for later use testing for unique users for each valid classification

        # Initialize variables and vector 
        class_count = 0
        aggregated_vector = [[0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
        row_counter = 0
        write_counter = 0

        for row2 in sorted_file:
            row_counter += 1
            new_subject = row2['subject_ids']
            new_user = {row2['user_name']}
            answer_vector = json.loads(row2['answer_vector'])
            if new_subject != subject:
                if row_counter != 1:  # don't want to output the empty initial values
                    write_counter += 1
                    new_row = {'subject_ids': subject,
                               'filename': filename,
                               'date': date,
                               'time': time,
                               'total_class': class_count,
                               'vote_fraction_vector': json.dumps(get_vote_fraction(class_count, aggregated_vector)),
                               'aggregated_vector': json.dumps(aggregated_vector)}
                    writer.writerow(new_row)
                subject = new_subject
                filename = row2['filename']
                date = row2['date']
                time = row2['time']
                users = new_user
                aggregated_vector = answer_vector
                class_count = 1

            else:
                if users != users | new_user:
                    users |= new_user
                    subject = new_subject
                    class_count += 1
                    # aggregation step:
                    for q in range(0, len(answer_vector)):
                        for r in range(0, len(answer_vector[q])):
                            aggregated_vector[q][r] += answer_vector[q][r]

        # catch the last aggregate after the end of the file is reached
        write_counter += 1
        new_row = {'subject_ids': subject,
                   'filename': filename,
                   'date': date,
                   'time': time,
                   'total_class': class_count,
                   'vote_fraction_vector': json.dumps(get_vote_fraction(class_count, aggregated_vector)),
                   'aggregated_vector': json.dumps(aggregated_vector)}
        writer.writerow(new_row)
    print(row_counter, 'lines aggregated into', write_counter, 'subject categories')


# _______________________________________________________________________________________________________________________
# This next section filters the responses for each subject to determine the final observation
# There is, like the final aggregated file, one line per subject
# Filters are explained in comments
# New simple filter is added on 04/24/2019 (see below)
# This is a "fork" - keep current version but try diff approach in a "fork"

# enumerate()
#   for an interable (ie anything with elements in it that can be worked on in such as lists or dictionaries)
#   this function returns a list of pairs of things for each element in the iterable, first being the index(lists)
#   or key (dictionaries) of the original element, and the second value being the element itself
#   ex: for list L = [1, 3, 5, 7], enumerate(L) = [(0,1), (1,3), (2,5), (3,7)]

# tuple
#   written (a, b, c) or a, b, c
#   simplified list with simplified methods and less structured
#   ex: below idx, el is a tuple, really (idx, el)

def simple_filter(vector):
    filtered_vector = []
    for b in range(0, len(vector)):
        filtered_vector.append(0)
    for idx, el in enumerate(vector):
        if float(el) >= .60:
            filtered_vector[idx] = 1
    return filtered_vector


with open(filtered_location, 'w', newline='') as fi_file:
    fieldnames = ['subject_ids',
                  'filename',
                  'date',
                  'time',
                  'temp',
                  'total_class',
                  'flag',
                  'nestlings',
                  'one_adult',
                  'two_adults',
                  'no_hawks',
                  '',
                  'brooding',
                  'arrive_depart',
                  'feeding',
                  'adult_eat',
                  'prey_location_change',
                  'prey_transfer',
                  'prey_visible_tinteractedwith',
                  'none_above',
                  '',
                  'vocal_nestling_no',
                  'vocal_nestling_peep',
                  'vocal_nestling_whistle',
                  'vocal_nestling_other',
                  'vocal_nestling_detected',
                  '',
                  'vocal_adult_no',
                  'vocal_adult_keear',
                  'vocal_adult_chwirk',
                  'vocal_adult_gank',
                  'vocal_adult_other',
                  'vocal_adult_detected',
                  '     ',
                  'vote_fraction_vector'
                  ]
    writer = csv.DictWriter(fi_file, fieldnames=fieldnames)
    writer.writeheader()

    with open(aggregate_location) as agg_file:
        aggregated_file = csv.DictReader(agg_file)
        rc6 = 0
        for row3 in aggregated_file:
            rc6 += 1
            subject = row3['subject_ids']
            filename = row3['filename']
            date = row3['date']
            time = row3['time']
            class_totals = row3['total_class']
            vote_fraction_vector = json.loads(row3['vote_fraction_vector'])
            new_line = {'subject_ids': subject,
                        'filename': filename,
                        'date': date,
                        'time': time,
                        'temp': '',
                        'total_class': class_totals,
                        'vote_fraction_vector': json.dumps(vote_fraction_vector)
                        }
            if int(class_totals) < 5:  # change to 5 when we want to know when the class totals are less than 5
                continue

            else:
                # if there are sufficient classifications we need to filter each piece of the vote fraction vector,
                # then assign the filtered results to the correct column.  The vote-fraction vector is formatted
                # q1_vector, q2_vector, q3_vector, q4_vector - 'who','what is happening','nestling_vocal','adult_vocal'

                # the 'who'
                for indx0, value0 in enumerate(simple_filter(vote_fraction_vector[0])):
                    # apply the simple filter to vote_fraction_vector[0]
                    # then assign the value to the correct column
                    new_line[fieldnames[indx0 + 7]] = value0
                # note the offset may change if the fieldnames are in a different order or some are added or removed

                # the 'what is happening'
                for indx1, value1 in enumerate(simple_filter(vote_fraction_vector[1])):
                    new_line[fieldnames[indx1 + 12]] = value1

                # the 'nestling vocalization'
                for indx2, value2 in enumerate(simple_filter(vote_fraction_vector[2])):
                    # then assign the actual filtered votes to the correct columns
                    new_line[fieldnames[indx2 + 21]] = value2

                # the 'adult vocalization'
                for indx3, value3 in enumerate(simple_filter(vote_fraction_vector[3])):
                    new_line[fieldnames[indx3 + 27]] = value3

                # code to flag problems with both no and some vocalizations both reaching consensus
                if new_line['vocal_nestling_no'] * new_line['vocal_nestling_detected'] == 1 \
                        or new_line['vocal_adult_no'] * new_line['vocal_adult_detected'] == 1:
                    new_line['flag'] = 1
                else:
                    new_line['flag'] = ''
                writer.writerow(new_line)

        print(rc6, 'subject-choices filtered')

        # note that in final file, a "1" indicates presence, "0" indicates absence
#  ___________________________________________________________________________________________________________________
